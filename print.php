<?php
include 'init.php';

$files = [];
$temp = scandir($cfg['printdir']);
foreach ($temp as $file) {
	if ($file == '.' || $file == '..') {
		continue;
	}
	if (strpos($file, '.pdf') !== FALSE) {
		$files[] = $file;
	}
}
view('Print Files', ['files' => $files], '', 'print');

