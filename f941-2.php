<?php
include 'init.php';
$s1 = fork('s1', 'P', 'index.php');
$pr = model('payroll');

// get data for report(s)
$data = $pr->get_941_data($_POST['year'], $_POST['quarter']);

// PDF report
$pdfr = model('forms');
$pdfr->form941($_POST['year'], $_POST['quarter'], $data, $cfg['printdir'] . 'form941.pdf');

// show it, baby!
view('Form 941 Display', ['f941' => $data], '', 'f941-2');

