<?php
include 'init.php';
$empid = fork('empid', 'G', 'emplist');
$pr = model('payroll');
$emp = $pr->get_employee($empid);

$mstatus_options = [
	['lbl' => 'Married/Joint', 'val' => 'M'],
	['lbl' => 'Single', 'val' => 'S'],
	['lbl' => 'Head of Household', 'val' => 'H']
];

$period_options = [
	['lbl' => 'Hourly', 'val' => 'H'],
	['lbl' => 'Daily', 'val' => 'D'],
	['lbl' => 'Weekly', 'val' => 'W'],
	['lbl' => 'Biweekly', 'val' => 'B'],
	['lbl' => 'Monthly', 'val' => 'M'],
	['lbl' => 'Quarterly', 'val' => 'Q'],
	['lbl' => 'Yearly', 'val' => 'Y']
];

$fields = [
	'empid' => [
		'name' => 'empid',
		'type' => 'hidden',
		'value' => $empid
	],
	'firstname' => [
		'name' => 'firstname',
		'type' => 'text',
		'size' => 35,
		'maxlength' => 35,
		'label' => 'First Name',
		'value' => $emp['firstname']
	],
	'middleinit' => [
		'name' => 'middleinit',
		'type' => 'text',
		'size' => 1,
		'maxlength' => 1,
		'label' => 'Middle Initial',
		'value' => $emp['middleinit']
	],
	'lastname' => [
		'name' => 'lastname',
		'type' => 'text',
		'size' => 35,
		'maxlength' => 35,
		'label' => 'Last Name',
		'value' => $emp['lastname']
	],
	'email' => [
		'name' => 'email',
		'type' => 'text',
		'size' => 50,
		'maxlength' => 50,
		'value' => $emp['email'],
		'label' => 'Email'
	],
	'ssno' => [
		'name' => 'ssno',
		'type' => 'text',
		'size' => 9,
		'maxlength' => 9,
		'label' => 'Social Security #',
		'value' => $emp['ssno']
	],
	'mstatus' => [
		'name' => 'mstatus',
		'type' => 'select',
		'options' => $mstatus_options,
		'label' => 'Filing Status',
		'value' => $emp['mstatus']
	],
	'deds' => [
		'name' => 'deds',
		'type' => 'text',
		'size' => 2,
		'maxlength' => 2,
		'label' => 'Deductions',
		'value' => $emp['deds']
	],
	'xfwt' => [
		'name' => 'xfwt',
		'type' => 'text',
		'size' => 12,
		'maxlength' => 12,
		'label' => 'Additional Withholding',
		'value' => $emp['xfwt']
	],
	'wage' => [
		'name' => 'wage',
		'type' => 'text',
		'size' => 12,
		'maxlength' => 12,
		'label' => 'Salary/Wage',
		'value' => $emp['wage']
	],
	'period' => [
		'name' => 'period',
		'type' => 'select',
		'options' => $period_options,
		'label' => 'Period',
		'value' => $emp['period']
	],
	's1' => [
		'name' => 's1',
		'type' => 'submit',
		'value' => 'Save'
	]
];
$form->set($fields);

view('Edit Employee', [], 'empedt2.php', 'empedt');


