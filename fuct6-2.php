<?php
include 'init.php';
$s1 = fork('s1', 'P', 'index.php');
$pr = model('payroll');

// get data for report(s)
$data = $pr->get_uct6_data($_POST['year'], $_POST['quarter']);

// PDF report
$pdfr = model('forms');
$pdfr->formuct6($_POST['year'], $_POST['quarter'], $data, $cfg['printdir'] . 'formuct6.pdf');

// show it, baby!
view('Form UCT6 Display', ['fuct6' => $data], '', 'fuct6-2');

