<?php
include 'init.php';
$pr = model('payroll');
$checks = $pr->get_paychecks();
if ($checks === FALSE) {
	emsg('F', 'No paychecks have been entered. You must generate one first.');
	redirect('chkgen.php');
}
view('Paychecks', ['checks' => $checks], '', 'chklist');

