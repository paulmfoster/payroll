<?php
include 'init.php';

$default_year = pdate::now()['y'];

$mstatus_options = [
	['lbl' => 'Head of Household', 'val' => 'H'],
	['lbl' => 'Married Filing Jointly', 'val' => 'M'],
	['lbl' => 'Not Applicable', 'val' => '*'],
	['lbl' => 'Single', 'val' => 'S']
];

$taxtype_options = [
	['lbl' => 'Federal Unemployment', 'val' => 'FUTA'],
	['lbl' => 'Federal Withholding', 'val' => 'FWT'],
	['lbl' => 'Medicare', 'val' => 'MEDI'],
	['lbl' => 'Social Security', 'val' => 'SSEC'],
	['lbl' => 'State Unemployment', 'val' => 'SUTA']
];

$emp_options = [
	['lbl' => 'Paid by Employee', 'val' => 'E'],
	['lbl' => 'Paid by Employer', 'val' => 'R']
];

$fields = [
	'year' => [
		'name' => 'year',
		'type' => 'text',
		'size' => 4,
		'maxlength' => 4,
		'value' => $default_year
	],
	'taxtype' => [
		'name' => 'taxtype',
		'type' => 'select', 
		'options' => $taxtype_options
	],
	'whopays' => [
		'name' => 'whopays',
		'type' => 'select',
		'options' => $emp_options
	],
	'mstatus' => [
		'name' => 'mstatus',
		'type' => 'select',
		'options' => $mstatus_options
	],
	'lolimit' => [
		'name' => 'lolimit',
		'type' => 'text',
		'size' => 12,
		'maxlength' => 12
	],
	'hilimit' => [
		'name' => 'hilimit',
		'type' => 'text',
		'size' => 12,
		'maxlength' => 12
	],
	'base' => [
		'name' => 'base',
		'type' => 'text',
		'size' => 9,
		'maxlength' => 9
	],
	'rate' => [
		'name' => 'rate',
		'type' => 'text',
		'size' => 9,
		'maxlength' => 9
	],
	's1' => [
		'name' => 's1',
		'type' => 'submit',
		'value' => 'Save'
	]
];
$form->set($fields);

view('Add To Tax Tables', [], 'tbladd2.php', 'tbladd');

