<?php
include 'init.php';
$pr = model('paycheck');
$emps = $pr->get_employees();
if ($emps === FALSE) {
	emsg('F', 'No employees found. You must first create one.');
	redirect('empadd.php');
}

$fields = [];
foreach ($emps as $emp) {
	$fields['qty_' . $emp['empid']] = [
		'name' => "qty_{$emp['empid']}",
		'type' => 'text',
		'size' => 5,
		'maxlength' => 5
	];
}
$fields['startdt'] = [
	'name' => 'startdt',
	'type' => 'date',
	'required' => 1
];
$fields['enddt'] = [
	'name' => 'enddt',
	'type' => 'date',
	'required' => 1
];
$fields['checkno'] = [
	'name' => 'checkno',
	'type' => 'text',
	'size' => 8,
	'maxlength' => 8,
	'required' => 1
];
$fields['s1'] = [
	'name' => 's1',
	'type' => 'submit',
	'value' => 'Generate'
];

$form->set($fields);

view('Generate Paychecks', ['emps' => $emps], 'chkgen2.php', 'chkgen');

