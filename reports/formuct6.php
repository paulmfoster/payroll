<?php

class formuct6 extends printq
{
    function centerx($text)
    {
        $x = round((8.5 * 72 - (strlen($text) * 7.2)) / 2, 0);
        return $x;
    }

    function print_number_line($x, $y, $label, $value)
    {
        $this->pdf->Text($x, $y, sprintf("%-48s %12.2f", $label, $value));
        return $y + 12;
    }

    function print_line($x, $y, $label, $value)
    {
        $this->pdf->Text($x, $y, sprintf("%-40s %20s", $label, $value));
        return $y + 12;
    }

    function formuct6($year, $qtr, $form)
    {
        $this->printq();
        $this->startup();

        $this->pdf->AddPage();
	
        $line = 12;
        $y = 72;
        $text = "Florida Form UCT-6 (Rev 03/12) for Quarter $qtr, Year $year";
        $this->pdf->Text($this->centerx($text), $y, $text); 
        $x = 72;
        $y += 2 * $line;
		foreach ($form as $f) {
			if ($f['empid'] == 'TOTAL') {
				$y = $this->print_number_line($x, $y, 'Line 2, Gross wages', $f['gross']);
				$y = $this->print_number_line($x, $y, 'Line 3, Gross wages over SUTA threshold', $f['nontaxable']);
				$y = $this->print_number_line($x, $y, 'Line 4, Taxable wages (Line 2 - Line 3)', $f['taxable']);
				$y = $this->print_number_line($x, $y, 'Line 9a, Tax due', $f['tax']);
				$y = $this->print_number_line($x, $y, 'Line 9b, Total tax due', $f['tax']);
				break;
			}
		}
		foreach ($form as $f) {
			if ($f['empid'] != 'TOTAL') {
				$y = $this->print_line($x, $y, 'Line 10, Soc Sec #', $f['ssno']);
				$y = $this->print_line($x, $y, 'Line 11, Employee name', $f['empname']);
				$y = $this->print_number_line($x, $y, 'Line 12a, Gross wages', $f['gross']);
				$y = $this->print_number_line($x, $y, 'Line 12b, Taxable wages', $f['taxable']);
			}	
        }
		foreach ($form as $f) {
			if ($f['empid'] == 'TOTAL') {
				$y = $this->print_number_line($x, $y, 'Line 13a, Total gross wages', $f['gross']);
				$y = $this->print_number_line($x, $y, 'Line 13b, Total taxable wages', $f['taxable']);
				break;
			}
		}

        // Actually output PDF
        $this->pdf->Output($this->printqdir . "/formuct6.pdf");
    }
};
