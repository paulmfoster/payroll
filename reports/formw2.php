<?php

class formw2 extends printq
{
    function centerx($text)
    {
        $x = round((8.5 * 72 - (strlen($text) * 7.2)) / 2, 0);
        return $x;
    }

    function print_number_line($x, $y, $label, $value)
    {
        $this->pdf->Text($x, $y, sprintf("%-48s %12.2f", $label, $value));
        return $y + 12;
    }

    function print_line($x, $y, $label, $value)
    {
        $this->pdf->Text($x, $y, sprintf("%-40s %20s", $label, $value));
        return $y + 12;
    }

    function formw2($year, $form)
    {
        $this->printq();
        $this->startup();

        for ($i = 0; $i < count($form); $i++) {
            $this->pdf->AddPage();

            $line = 12;
            $y = 72;
            $text = "IRS Form W2 for Year $year";
            $this->pdf->Text($this->centerx($text), $y, $text); 
            $x = 72;
            $y += 2 * $line;
            $y = $this->print_line($x, $y, 'Line b, FEI Number', '59-3383045');
            $this->pdf->Text($x, $y, 'Line c, (Enter company name and address)');
            $y += 12;
            $y = $this->print_line($x, $y, 'Line d, Social Security #', $form[$i]['ssno']);
            $y = $this->print_line($x, $y, 'Line e, Employee Name', $form[$i]['empname']);
            $y = $this->print_number_line($x, $y, 'Line 1, Gross Wages', $form[$i]['gross']);
            $y = $this->print_number_line($x, $y, 'Line 2, Federal Withholding', $form[$i]['fwt']);
            $y = $this->print_number_line($x, $y, 'Line 3, Soc. Sec. Wages', $form[$i]['gross']);
            $y = $this->print_number_line($x, $y, 'Line 4, Soc. Sec. Taxes', $form[$i]['ssec']);
            $y = $this->print_number_line($x, $y, 'Line 5, Medicare Wages', $form[$i]['gross']);
            $y = $this->print_number_line($x, $y, 'Line 6, Medicare Taxes', $form[$i]['medi']);
        }
        // Actually output PDF
        $this->pdf->Output($this->printqdir . "/formw2.pdf");
    }
};

