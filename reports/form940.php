<?php

class form940 extends printq
{
    function centerx($text)
    {
        $x = round((8.5 * 72 - (strlen($text) * 7.2)) / 2, 0);
        return $x;
    }

    function print_number_line($x, $y, $label, $value)
    {
        $this->pdf->Text($x, $y, sprintf("%-48s %12.2f", $label, $value));
        return $y + 12;
    }

    function print_line($x, $y, $label, $value)
    {
        $this->pdf->Text($x, $y, sprintf("%-48s %12s", $label, $value));
        return $y + 12;
    }

    function form940($year, $form)
    {
        $this->printq();
        $this->startup();

        $this->pdf->AddPage();

        $line = 12;
        $y = 72;
        $text = "IRS Form 940 for Year $year";
        $this->pdf->Text($this->centerx($text), $y, $text); 
        $x = 72;
        $y += 2 * $line;
        $y = $this->print_line($x, $y, 'Part 1, Line 1a, State', 'FL');
        $y = $this->print_number_line($x, $y, 'Part 2, Line 3, Total payments', $form['gross']);
        $y = $this->print_number_line($x, $y, 'Part 2, Line 5, Pmts over FUTA limit', $form['exempt']);
        $y = $this->print_number_line($x, $y, 'Part 2, Line 6, (Same as above)', $form['exempt']);
        $y = $this->print_number_line($x, $y, 'Part 2, Line 7, Total taxable FUTA wages', $form['taxable']);
        $y = $this->print_number_line($x, $y, 'Part 2, Line 8, Line#7 * FUTA rate', $form['tax']);
        $y = $this->print_number_line($x, $y, 'Part 4, Line 12, (Same as Line#8)', $form['tax']);
        $y = $this->print_number_line($x, $y, 'Part 4, Line 13, Deposits (Same as above)', $form['tax']);

        // Actually output PDF
        $this->pdf->Output($this->printqdir . "/form940.pdf");
    }
};

