<?php

class form941 extends printq
{
    function centerx($text)
    {
        $x = round((8.5 * 72 - (strlen($text) * 7.2)) / 2, 0);
        return $x;
    }

    function print_number_line($x, $y, $label, $value)
    {
        $this->pdf->Text($x, $y, sprintf("%-48s %12.2f", $label, $value));
        return $y + 12;
    }

    function print_line($x, $y, $label, $value)
    {
        $this->pdf->Text($x, $y, sprintf("%-48s %12s", $label, $value));
        return $y + 12;
    }

    function form941($year, $qtr, $form)
    {
        $this->printq();
        $this->startup();

        $this->pdf->AddPage();

        $line = 12;
        $y = 72;
        $text = "IRS Form 941 for Quarter $qtr, Year $year";
        $this->pdf->Text($this->centerx($text), $y, $text); 
        $x = 72;
        $y += 2 * $line;
        $y = $this->print_line($x, $y, 'Line 1, Number of emps on 12th day of qtr', $form['nemps']);
        $y = $this->print_number_line($x, $y, 'Line 2, Gross wages', $form['gross']);
        $y = $this->print_number_line($x, $y, 'Line 3, Fed W/H taxes', $form['fwt']);
        $y = $this->print_number_line($x, $y, 'Line 5a, Column 2, Soc Sec taxes', $form['ssec']);
        $y = $this->print_number_line($x, $y, 'Line 5c, Column 2, Medicare taxes', $form['medi']);
        $y = $this->print_number_line($x, $y, 'Line 5d, Soc Sec + Medicare taxes', $form['ssec_medi']);
        $y = $this->print_number_line($x, $y, 'Line 6, Total taxes', $form['total_taxes']);
        $y = $this->print_number_line($x, $y, 'Line 7a, Fractions of cents adjustment', $form['cent_adj']);
        $y = $this->print_number_line($x, $y, 'Line 7h, Same as above', $form['cent_adj']);
        $y = $this->print_number_line($x, $y, 'Line 8, Taxes plus/minus adjustments', $form['collected_taxes']);
        $y = $this->print_number_line($x, $y, 'Line 10, Same as above', $form['collected_taxes']);
        $y = $this->print_number_line($x, $y, 'Line 11, Deposits this quarter', $form['collected_taxes']);
        $y = $this->print_number_line($x, $y, 'Line 12, Balance due', 0.00);
        $y = $this->print_line($x, $y, 'Line 14, This state', 'FL');
        $y = $this->print_number_line($x, $y, 'Line 15, First month liability', $form['liab1']);
        $y = $this->print_number_line($x, $y, 'Line 15, Second month liability', $form['liab2']);
        $y = $this->print_number_line($x, $y, 'Line 15, Third month liability', $form['liab3']);
        $y = $this->print_number_line($x, $y, 'Line 15, Total liability', $form['collected_taxes']);

        // Actually output PDF
        $this->pdf->Output($this->printqdir . "/form941.pdf");
    }
};

