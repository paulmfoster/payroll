<?php

class formw3 extends printq
{
    function centerx($text)
    {
        $x = round((8.5 * 72 - (strlen($text) * 7.2)) / 2, 0);
        return $x;
    }

    function print_number_line($x, $y, $label, $value)
    {
        $this->pdf->Text($x, $y, sprintf("%-48s %12.2f", $label, $value));
        return $y + 12;
    }

    function print_line($x, $y, $label, $value)
    {
        $this->pdf->Text($x, $y, sprintf("%-40s %20s", $label, $value));
        return $y + 12;
    }

    function formw3($year, $form)
    {
        $this->printq();
        $this->startup();

        $this->pdf->AddPage();

        $line = 12;
        $y = 72;
        $text = "IRS Form W3 for Year $year";
        $this->pdf->Text($this->centerx($text), $y, $text); 
        $x = 72;
        $y += 2 * $line;
        $y = $this->print_line($x, $y, 'Section b, Kind of payer', '941');
        $y = $this->print_line($x, $y, 'Section c, Number of W2s', $form['nemps']);
        $y = $this->print_line($x, $y, 'Section e, FEI Number', '59-3383045');
        $this->pdf->Text($x, $y, 'Section f, (Enter company name and address)');
        $y += 12;
        $y = $this->print_number_line($x, $y, 'Line 1, Gross Wages', $form['gross']);
        $y = $this->print_number_line($x, $y, 'Line 2, Federal Withholding', $form['fwt']);
        $y = $this->print_number_line($x, $y, 'Line 3, Soc. Sec. Wages', $form['gross']);
        $y = $this->print_number_line($x, $y, 'Line 4, Soc. Sec. Taxes', $form['ssec']);
        $y = $this->print_number_line($x, $y, 'Line 5, Medicare Wages', $form['gross']);
        $y = $this->print_number_line($x, $y, 'Line 6, Medicare Taxes', $form['medi']);
        $this->pdf->Text($x, $y, 'Final Section, (Enter Contact Information)');

        // Actually output PDF
        $this->pdf->Output($this->printqdir . "/formw3.pdf");
    }
};

