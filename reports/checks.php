<?php 


class checks extends printq
{
    function check_stub($xy, $hdr, $dtl, $offset)
    {
        $y = $xy['stubvend_y'] + ($offset * 12);
        $x = $xy['stubvend_x'];

        $stubvend = sprintf('Vendor No: %s / Name: %s', trim($hdr['vendno']), trim($hdr['company']));
        $this->pdf->Text($x, $y, $stubvend);

        $y = $xy['stubckno_y'] + ($offset * 12);
        $x = $xy['stubckno_x'];

        $this->pdf->Text($x, $y, $hdr['checkno']);

        $y += 12;
        $x = $xy['stubvend_x'];

        $this->pdf->Text($x, $y,  ' Invoice          Reference         Inv Date   Inv Amount    Amt Paid');

        $y += 12;
        foreach ($dtl as $rec) {
            if ($rec['vendno'] == $hdr['vendno']) {
                $stub = sprintf('%-10s   %-20s   %8s   %10s   %10s', $rec['billno'], $rec['reference'], $rec['billdate'], number_format($rec['amount'], 2), number_format($rec['aprpay'], 2));
                $this->pdf->Text($x, $y, $stub);
                $y += 12;
            }
        }

        $this->pdf->Text($x, $y, '                                                            ==========');

        $y += 12;
        $totals = sprintf('Check Date: %8s                           Check Total: %10s', $hdr[checkdate], number_format($hdr['total'], 2));
        $this->pdf->Text($x, $y, $totals);
    }

    /* ========================================================================
       This routine populates the check PDF with the proper values.
       It is passed three arrays:
       $xy = an array of coordinates for items on the check
       drawn from the apcklayout table.
       $hdr = an array of header-type information, like the
       vendor number, check number, date, etc.
       $dtl = an array of bills this check is paying. This
       information is printed on the stubs.
       ======================================================================== */

    function checks($xy, $hdr, $dtl)
    {
        $this->printq();
        $this->startup();

        foreach ($hdr as $h) {

            $this->pdf->AddPage();
            $this->check_stub($xy, $h, $dtl, 0);

            // ===== Main Check Body

            $this->pdf->Text($xy['cknum_x'], $xy['cknum_y'], $h['checkno']);

            $this->pdf->Text($xy['amtspelled_x'], $xy['amtspelled_y'], $h['amtspelled']);

            $this->pdf->Text($xy['ckdate_x'], $xy['ckdate_y'], $h['checkdate']);

            $this->pdf->Text($xy['ckamt_x'], $xy['ckamt_y'], number_format($h['total'], 2));

            $this->pdf->Text($xy['vendor_x'], $xy['vendor_y'], $h['company']);
            $y = $xy['vendor_y'] + 12;
            if ($h['attn'] != '') {
                $this->pdf->Text($xy['vendor_x'], $y, $h['attn']);
                $y += 12;
            }
            if ($h['address1'] != '') {
                $this->pdf->Text($xy['vendor_x'], $y, $h['address1']);
                $y += 12;
            }
            if ($h['address2'] != '') {
                $this->pdf->Text($xy['vendor_x'], $y, $h['address2']);
                $y += 12;
            }
            if ($h['city'] != '') {
                $this->pdf->Text($xy['vendor_x'], $y, $h['city'] . ', ' . $h['state'] . '  ' . $h['zip']);
                $y += 12;
            }
            if ($h['acctno'] != '') {
                $this->pdf->Text($xy['vendor_x'], $y, $h['acctno']);
            }

            // ===== End Main Check Body

            $this->check_stub($xy, $h, $dtl, $xy['cklines']);
        }

        // Actually output PDF
        $this->pdf->Output($this->printqdir . "/checks.pdf");
    }
};
