<?php
include 'init.php';
$id = fork('id', 'G', 'tblshow.php');
$pr = model('payroll');
$tax = $pr->get_single_tax($id);

$mstatus_options = [
	['lbl' => 'Head of Household', 'val' => 'H'],
	['lbl' => 'Married Filing Jointly', 'val' => 'M'],
	['lbl' => 'Not Applicable', 'val' => '*'],
	['lbl' => 'Single', 'val' => 'S']
];

$taxtype_options = [
	['lbl' => 'Federal Unemployment', 'val' => 'FUTA'],
	['lbl' => 'Federal Withholding', 'val' => 'FWT'],
	['lbl' => 'Medicare', 'val' => 'MEDI'],
	['lbl' => 'Social Security', 'val' => 'SSEC'],
	['lbl' => 'State Unemployment', 'val' => 'SUTA']
];

$emp_options = [
	['lbl' => 'Paid by Employee', 'val' => 'E'],
	['lbl' => 'Paid by Employer', 'val' => 'R']
];

$fields = [
	'id' => [
		'name' => 'id',
		'type' => 'hidden',
		'value' => $tax['id']
	],
	'year' => [
		'name' => 'year',
		'type' => 'text',
		'size' => 4,
		'maxlength' => 4,
		'value' => $tax['year']
	],
	'taxtype' => [
		'name' => 'taxtype',
		'type' => 'select', 
		'options' => $taxtype_options,
		'value' => $tax['taxtype']
	],
	'whopays' => [
		'name' => 'whopays',
		'type' => 'select',
		'options' => $emp_options,
		'value' => $tax['whopays']
	],
	'mstatus' => [
		'name' => 'mstatus',
		'type' => 'select',
		'options' => $mstatus_options,
		'value' => $tax['mstatus']
	],
	'lolimit' => [
		'name' => 'lolimit',
		'type' => 'text',
		'size' => 12,
		'maxlength' => 12,
		'value' => $tax['lolimit']
	],
	'hilimit' => [
		'name' => 'hilimit',
		'type' => 'text',
		'size' => 12,
		'maxlength' => 12,
		'value' => $tax['hilimit']
	],
	'base' => [
		'name' => 'base',
		'type' => 'text',
		'size' => 9,
		'maxlength' => 9,
		'value' => $tax['base']
	],
	'rate' => [
		'name' => 'rate',
		'type' => 'text',
		'size' => 9,
		'maxlength' => 9,
		'value' => $tax['rate'] * 100
	],
	's1' => [
		'name' => 's1',
		'type' => 'submit',
		'value' => 'Save Edits'
	]
];
$form->set($fields);

view('Edit Tax Table Rule', [], 'tbledt2.php', 'tbledt');

