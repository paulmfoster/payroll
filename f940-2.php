<?php
include 'init.php';
$s1 = fork('s1', 'P', 'index.php');
$pr = model('payroll');

// get data for report(s)
$data = $pr->get_940_data($_POST['year']);

// PDF report
$pdfr = model('forms');
$pdfr->form940($_POST['year'], $data, $cfg['printdir'] . 'form940.pdf');

// show it, baby!
view('Form 940 Display', ['f940' => $data], '', 'f940-2');

