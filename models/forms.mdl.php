<?php

class forms
{
    public $pdfr;

	function __construct()
	{
		global $cfg;

		$this->pdfr = library('pdf_report');
	}

	function nf($nbr)
	{
		$str = sprintf('%12.2f', number_format($nbr, 2, '.', ''));
		return $str;
	}

	function form8109($month, $year, $form, $filename)
	{
		$p = $this->pdfr;
		$p->add_page();
        $p->set_line(6);
        $p->set_column(10);

		$text = "IRS Form 8109 for Month $month, Year $year";
		$p->center($text);
		$p->skip_line();
		$p->print_line('Gross                    ' . $this->nf($form['gross']));
		$p->print_line('Federal Withholding      ' . $this->nf($form['fwt']));
		$p->print_line('Extra Withholding        ' . $this->nf($form['xfwt']));
		$fwt = $form['fwt'] + $form['xfwt'];
		$p->print_line('Total Withholding        ' . $this->nf($fwt));
		$p->print_line('Employee Social Security ' . $this->nf($form['essec']));
		$p->print_line('Employer Social Security ' . $this->nf($form['rssec']));
		$p->print_line('Employee Medicare        ' . $this->nf($form['emedi']));
		$p->print_line('Employer Medicare        ' . $this->nf($form['rmedi']));
		$p->print_line('State Unemployment       ' . $this->nf($form['suta']));
		$p->print_line('Federal Unemployment     ' . $this->nf($form['futa']));
		$p->print_line('Net                      ' . $this->nf($form['net']));
		$p->print_line('Total Tax Deposit        ' . $this->nf($form['taxes']));

		$p->output($filename);
	}

    function form941($year, $qtr, $form, $filename)
    {
		$p = $this->pdfr;
		$p->add_page();
        $p->set_line(6);
        $p->set_column(10);

        $text = "IRS Form 941 for Quarter $qtr, Year $year";
		$p->center($text);
		$p->skip_line();

		$p->print_line('Number of employees on 12th day of quarter ' . $this->nf($form['nemps']));
		$p->print_line('Gross Wages                                ' . $this->nf($form['gross']));
		$p->print_line('Federal Withholding Taxes                  ' . $this->nf($form['fwt']));
		$p->print_line('Social Security Taxes (calc)               ' . $this->nf($form['ssec']));
		$p->print_line('Medicare Taxes (calc)                      ' . $this->nf($form['medi']));
		$p->print_line('Social Security + Medicare Taxes (calc)    ' . $this->nf($form['ssec_medi']));
		$p->print_line('Total Taxes (calc)                         ' . $this->nf($form['total_taxes']));
		$p->print_line('Fractions of Cents Adjustment              ' . $this->nf($form['cent_adj']));
		$p->print_line('Taxes plus/minus adjustments (actual)      ' . $this->nf($form['collected_taxes']));
		$p->print_line('Deposits This Quarter                      ' . $this->nf($form['collected_taxes']));
		$p->print_line('Balance Due                                ' . $this->nf(0));
		$p->print_line('This State                                 ' . '          FL');
		$p->print_line('First Month Liability                      ' . $this->nf($form['liab1']));
		$p->print_line('Second Month Liability                     ' . $this->nf($form['liab2']));
		$p->print_line('Third Month Liability                      ' . $this->nf($form['liab3']));
		$p->print_line('Total Liability                            ' . $this->nf($form['collected_taxes']));

		$p->output($filename);

    }

    function formuct6($year, $qtr, $form, $filename)
    {
		$p = $this->pdfr;
		$p->add_page();
        $p->set_line(6);
        $p->set_column(10);

        $text = "Florida Form UCT-6 (Rev 01/11) Quarter $qtr, Year $year";
		$p->center($text);
		$p->skip_line();
	
		foreach ($form as $f) {
			if ($f['empid'] == 'TOTAL') {
				$p->print_line('Gross wages                          ' . $this->nf($f['gross']));
				$p->print_line('Gross wages over SUTA threshold      ' . $this->nf($f['nontaxable']));
				$p->print_line('Taxable wages (Line 2 - Line 3)      ' . $this->nf($f['taxable']));
				$p->print_line('Tax due                              ' . $this->nf($f['tax']));
				$p->print_line('Total tax due                        ' . $this->nf($f['tax']));
				break;
			}
		}
		foreach ($form as $f) {
			if ($f['empid'] != 'TOTAL') {
				$p->print_line('Soc Sec #      ' . $f['ssno'], TRUE);
				$p->print_line('Employee name  ' . $f['empname'], TRUE);
				$p->print_line('Gross wages                          ' . $this->nf($f['gross']), TRUE);
				$p->print_line('Taxable wages                        ' . $this->nf($f['taxable']), TRUE);
			}	
        }
		foreach ($form as $f) {
			if ($f['empid'] == 'TOTAL') {
				$p->print_line('Total gross wages                    ' . $this->nf($f['gross']), TRUE);
				$p->print_line('Total taxable wages                  ' . $this->nf($f['taxable']), TRUE);
				break;
			}
		}

		$p->output($filename);
    }

    function form940($year, $form, $filename)
    {
		$p = $this->pdfr;
		$p->add_page();
        $p->set_line(6);
        $p->set_column(10);

        $text = "IRS Form 940 for Year $year";
		$p->center($text);
		$p->skip_line();

        $p->print_line('State                                                          FL');
        $p->print_line('Total payments                                       ' . $this->nf($form['gross']));
   	    $p->print_line('Pmts over FUTA limit                                 ' . $this->nf($form['exempt']));
        $p->print_line('Total taxable FUTA wages                             ' . $this->nf($form['taxable']));
        $p->print_line('Taxable FUTA wages * FUTA rate                       ' . $this->nf($form['tax']));

		$p->output($filename);
    }

    function formw2($year, $form, $filename)
    {
		$p = $this->pdfr;
		
		for ($i = 0; $i < count($form); $i++) {
			$p->add_page();
            $p->set_line(6);
            $p->set_column(10);

			$text = "IRS Form W-2 for Year $year";
			$p->center($text);
			$p->skip_line();

			$p->print_line('FEI Number                                      59-3383045');
			$p->print_line('Social Security #                               ' . $form[$i]['ssno']);
			$p->print_line('Employee Name                                   ' . $form[$i]['empname']);
			$p->print_line('Gross Wages                                     ' . $this->nf($form[$i]['gross']));
			$p->print_line('Federal Withholding                             ' . $this->nf($form[$i]['fwt']));
			$p->print_line('Soc. Sec. Wages                                 ' . $this->nf($form[$i]['gross']));
			$p->print_line('Soc. Sec. Taxes                                 ' . $this->nf($form[$i]['ssec']));
			$p->print_line('Medicare Wages                                  ' . $this->nf($form[$i]['gross']));
			$p->print_line('Medicare Taxes                                  ' . $this->nf($form[$i]['medi']));
        }

		$p->output($filename);
    }

    function formw3($year, $form, $filename)
    {
		$p = $this->pdfr;
		$p->add_page();
        $p->set_line(6);
        $p->set_column(10);

        $text = "IRS Form W3 for Year $year";
		$p->center($text);
		$p->skip_line();

        $p->print_line('Kind of payer                                            941');
        $p->print_line('Number of W2s                                              ' . $form['nemps']);
        $p->print_line('FEI Number                                        59-3383045');

		$p->print_line('Gross Wages                                     ' . $this->nf($form['gross']));
		$p->print_line('Federal Withholding                             ' . $this->nf($form['fwt']));
		$p->print_line('Soc. Sec. Wages                                 ' . $this->nf($form['gross']));
		$p->print_line('Soc. Sec. Taxes                                 ' . $this->nf($form['ssec']));
		$p->print_line('Medicare Wages                                  ' . $this->nf($form['gross']));
		$p->print_line('Medicare Taxes                                  ' . $this->nf($form['medi']));

		$p->output($filename);
    }
};



