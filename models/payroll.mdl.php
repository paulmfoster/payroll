<?php

// FIXME: no separate table for deduction allowances
// FIXME: no way to delete a check and its data

class payroll
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

	/**
	 * Get totals for each taxtype for all employees or for the employee in question,
	 * from one date to another (typically YTD).
	 *
	 * @param string Employee ID
	 * @param date From date (before our interval)
	 * @param date To date (after our interval)
	 * @return array Sum of each tax to date
	 */

    function get_paycheck_totals($empid, $from, $to)
    {
        if ($empid == '*') {
	        $sql = "select sum(futa) as futa, sum(suta) as suta, sum(emedi) as emedi, sum(rmedi) as rmedi, sum(essec) as essec, sum(rssec) as rssec, sum(gross) as gross, sum(fwt) as fwt, sum(xfwt) as xfwt, sum(net) as net from paychecks where checkdate > '$from' and checkdate < '$to'";
		}
        else {
    	    $sql = "select sum(futa) as futa, sum(suta) as suta, sum(emedi) as emedi, sum(rmedi) as rmedi, sum(essec) as essec, sum(rssec) as rssec, sum(gross) as gross, sum(fwt) as fwt, sum(xfwt) as xfwt, sum(net) as net from paychecks where empid = '$empid' and checkdate > '$from' and checkdate < '$to'";
		}
        $arr = $this->db->query($sql)->fetch();
        return $arr;
    }

	/**
	 * Get tax tables for all taxes for a given year.
	 */

    function get_taxes_by_year($year)
    {
        $sql = "SELECT * FROM taxes WHERE year = $year ORDER BY taxtype, mstatus, base, lolimit";
        $arr = $this->db->query($sql)->fetch_all();
        return $arr;
    }

	function get_tax_tables_by_type($year)
	{
        $sql = "SELECT * FROM taxes WHERE year = $year ORDER BY taxtype, mstatus, base, lolimit";
        $tables = $this->db->query($sql)->fetch_all();

		$tbl = [];

		foreach ($tables as $table) {
			$taxtype = $table['whopays'] . $table['taxtype'];
			$tbl[$taxtype] = $table;
		}

		return $tbl;
	}


	function get_paychecks()
	{
		$sql = "SELECT * FROM paychecks ORDER BY checkdate, checkno, empid";
		$recs = $this->db->query($sql)->fetch_all();
		return $recs;
	}


    function get_months()
    {
        $arr = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        return $arr;
    }

    function get_years()
    {
		$sql = "SELECT DISTINCT checkdate FROM paychecks ORDER BY checkdate";
		$dates = $this->db->query($sql)->fetch_all();
		if ($dates === FALSE) {
			return FALSE;
		}

		$last_year = '';
		$years = [];
		foreach ($dates as $date) {
			$year = substr($date['checkdate'], 0, 4);
			if ($year != $last_year) {
				$years[] = $year;
				$last_year = $year;
			}
		}
		return $years;
    }

	/**
	 * Sort prtax records for use in modifying the tables.
	 */

    function prsort($a, $b)
    {
        if ($a['year'] < $b['year'])
            return -1;
        elseif ($a['year'] > $b['year'])
            return 1;
        else {
            if ($a['taxtype'] < $b['taxtype'])
                return -1;
            elseif ($a['taxtype'] > $b['taxtype'])
                return 1;
            else {
                if ($a['mstatus'] < $b['mstatus'])
                    return -1;
                elseif ($a['mstatus'] > $b['mstatus'])
                    return 1;
                else {
                    if ($a['base'] < $b['base'])
                        return -1;
                    elseif ($a['base'] > $b['base'])
                        return 1;
                    else {
                        if ($a['lolimit'] < $b['lolimit'])
                            return -1;
                        elseif ($a['lolimit'] > $b['lolimit'])
                            return 1;
                        else
                            return 0;
                    }
                }
            }
        }
    }

	/**
	 * Call up the last year of taxes records.
	 */

    function get_latest_taxes()
    {
        $sql = "SELECT max(year) AS year FROM taxes";
        $y = $this->db->query($sql)->fetch();
		$year = $y['year'];

        $sql = "SELECT * FROM taxes WHERE year = $year ORDER BY year, taxtype, mstatus, base, lolimit";
        $exp = $this->db->query($sql)->fetch_all();

        return $exp;
    }

	function get_single_tax($id)
	{
		$sql = "SELECT * FROM taxes WHERE id = $id";
		$rec = $this->db->query($sql)->fetch();
		return $rec;
	}
	/**
	 * Save changes to taxes table for a given record.
	 */

    function save_prtax_changes($post)
    {
		$id = explode(',', $post['tidstr']);
		foreach ($id as $k) {
			$rec = [
				'year' => $post['year'],
				'taxtype' => $post["taxtype_$k"],
				'mstatus' => $post["mstatus_$k"],
				'lolimit' => $post["lolimit_$k"],
				'hilimit' => $post["hilimit_$k"],
				'base' => $post["base_$k"],
				'rate' => $post["rate_$k"]
			];
			$this->db->update('taxes', $rec, "id = $k");
		}
    }

	function save_tax_edit($post)
	{
		$post['rate'] = $post['rate'] / 100;
		$prec = $this->db->prepare('taxes', $post);
		$this->db->update('taxes', $prec, "id = {$post['id']}");
		return TRUE;
	}

	/**
	 * Insert new records into taxes table.
	 */

    function add_prtax_values($post)
    {
		$post['rate'] = $post['rate'] / 100;
		$prec = $this->db->prepare('taxes', $post);
		$this->db->insert('taxes', $prec);
    }

	function delete_prtax_values($id)
	{
		$this->db->delete('taxes', "id = $id");
	}

	function get_employee($empid)
	{
		$sql = "SELECT * FROM employees WHERE empid = '$empid'";
		$emp = $this->db->query($sql)->fetch();
		return $emp;
	}

    function get_employees()
    {
        $sql = "SELECT * FROM employees ORDER BY empid";
        $arr = $this->db->query($sql)->fetch_all();
        return $arr;
    }

	function delete_employee($empid)
	{
		$sql = "SELECT * FROM paychecks WHERE empid = '$empid'";
		$rec = $this->db->query($sql)->fetch();
		if ($rec === FALSE) {
			$this->db->delete('employees', "empid = '$empid'");
			return FALSE;
		}
		else {
			emsg('F', 'Employee has paychecks already.');
		}
		return TRUE;
	}

	function add_employee($post)
	{
		$sql = "SELECT * FROM employees WHERE empid = '{$post['empid']}'";
		$result = $this->db->query($sql)->fetch();
		if ($result !== FALSE) {
			emsg('F', 'Attempt to add duplicate employee (same ID).');
			return FALSE;
		}

		$post['empname'] = $post['firstname'] . ' ' . $post['middleinit'] . ' ' . $post['lastname'];

		if (empty($post['deds'])) {
			$post['deds'] = 0;
		}
		if (empty($post['xfwt'])) {
			$post['xfwt'] = 0;
		}

		$prec = $this->db->prepare('employees', $post);
		$this->db->insert('employees', $prec);
		emsg('S', 'Employee successfully added.');
		return TRUE;

	}

	function update_employee($post)
	{
		if (empty($post['xfwt'])) {
			$post['xfwt'] = 0;
		}
		if (empty($post['deds'])) {
			$post['deds'] = 0;
		}

		$prec = $this->db->prepare('employees', $post);
		$this->db->update('employees', $prec, "empid = '{$post['empid']}'");
		return TRUE;
	}

	/**
	 * Get gross and tax totals, all employees for a month.
	 *
	 * The form 8109 is deprecated as of 2011, but represents the gross and tax totals
	 * for all employees for a month.
	 */

    function get_8109_data($year, $month)
    {
		$ofrom = pdate::fromints($year, $month, 1);
		$from = pdate::toiso($ofrom);
		$last_day = pdate::days_in_month($month, $year);
		$oto = pdate::fromints($year, $month, $last_day);
		$to = pdate::toiso($oto);

        $sql = "SELECT sum(gross) as gross, sum(fwt) as fwt, sum(xfwt) as xfwt, sum(essec) as essec, sum(rssec) as rssec, sum(emedi) as emedi, sum(rmedi) as rmedi, sum(suta) as suta, sum(futa) as futa, sum(net) as net, sum(fwt + xfwt + essec + rssec + emedi + rmedi) as taxes from paychecks where checkdate > '$from' and checkdate < '$to'";
        $a = $this->db->query($sql)->fetch();
        return $a;
    }

    function get_941_data($year, $qtr)
    {
        $ret = array();

		// first month of quarter
		$first_month = $qtr + ($qtr - 1) * 2;

		// get from and to dates
        $dt = pdate::fromints($year, $first_month, 1);
        $dfrom = pdate::day_before_quarter($dt);
        $from = pdate::toiso($dfrom);
        $dto = pdate::day_after_quarter($dt);
        $to = pdate::toiso($dto);

		// get number of employees for this interval
        $qry = "SELECT DISTINCT empid FROM paychecks WHERE checkdate > '$from' AND checkdate < '$to'";
        $ret = $this->db->query($qry)->fetch_all();
		$ret['nemps'] = count($ret);

		// get payroll data for interval
        // $form = $this->get_paycheck_totals('*', $from, $to);
	    $sql = "select sum(emedi) as emedi, sum(rmedi) as rmedi, sum(essec) as essec, sum(rssec) as rssec, sum(gross) as gross, sum(fwt) as fwt, sum(xfwt) as xfwt from paychecks where checkdate > '$from' and checkdate < '$to'";
		$form = $this->db->query($sql)->fetch();

        $ret['gross'] = $form['gross'];
        $ret['fwt'] = $form['fwt'] + $form['xfwt'];

		$ret['emedi'] = $form['emedi'];
		$ret['rmedi'] = $form['rmedi'];
		$ret['essec'] = $form['essec'];
		$ret['rssec'] = $form['rssec'];

		// IRS Form 941 has a "fractions of cents" calculation. It's not
		// enough to just accumulate the amounts we reserved. We have to do
		// the calculations all over again and compare them to what we
		// reserved. This applies to medicare and social security.

		// medicare

		$sql = "SELECT rate from taxes WHERE year = $year AND taxtype = 'MEDI' AND whopays = 'R'";
		$result = $this->db->query($sql)->fetch();
		$rmedi_rate = $result['rate'];

		$sql = "SELECT rate from taxes WHERE year = $year AND taxtype = 'MEDI' AND whopays = 'E'";
		$result = $this->db->query($sql)->fetch();
		$emedi_rate = $result['rate'];

		$ret['medi'] = bcadd(bcmul($emedi_rate, $ret['gross'], 2), bcmul($rmedi_rate, $ret['gross'], 2), 2);

		// social security

		$sql = "SELECT rate from taxes WHERE year = $year AND taxtype = 'SSEC' AND whopays = 'R'";
		$result = $this->db->query($sql)->fetch();
		$rssec_rate = $result['rate'];

		$sql = "SELECT rate from taxes WHERE year = $year AND taxtype = 'SSEC' AND whopays = 'E'";
		$result = $this->db->query($sql)->fetch();
		$essec_rate = $result['rate'];

		$ret['ssec'] = bcadd(bcmul($essec_rate, $ret['gross'], 2), bcmul($rssec_rate, $ret['gross'], 2), 2);
	
		// derived numbers

		// calculated total of ssec and medi
        $ret['ssec_medi'] = bcadd($ret['ssec'], $ret['medi'], 2);
		// calculated total of taxes collected
        $ret['total_taxes'] = bcadd($ret['ssec_medi'], $ret['fwt'], 2);
		// accumulated total taxes from paychecks
        $ret['collected_taxes'] = bcadd($form['fwt'], bcadd($form['xfwt'], bcadd($form['essec'], bcadd($form['rssec'], bcadd($form['emedi'], $form['rmedi'], 2), 2), 2), 2), 2);
		// how much ssec/medi was withheld
		$ssec_medi = bcadd($form['emedi'], bcadd($form['rmedi'], bcadd($form['essec'], $form['rssec'], 2), 2), 2);
		// difference between the calculated sums versus what was withheld
		// if we withheld too much, this would be negative
		// if we withheld too little, this would be positive
        $ret['cent_adj'] = bcsub($ssec_medi, $ret['ssec_medi'], 2);

		// tax liabilities for each month

		$qmonths = [
			1 => [1, 2, 3],
			2 => [4, 5, 6],
			3 => [7, 8, 9],
			4 => [10, 11, 12]
		];

        for ($j = 0; $j < 3; $j++) {

            $first_of_month = pdate::fromints($year, $qmonths[$qtr][$j], 1);
            $dfrom = pdate::day_before_month($first_of_month);
            $from = pdate::toiso($dfrom);
            $dto = pdate::day_after_month($first_of_month);
            $to = pdate::toiso($dto);

            $sql = "SELECT sum(fwt) + sum(xfwt) + sum(essec) + sum(rssec) + sum(emedi) + sum(rmedi) as taxes FROM paychecks WHERE checkdate > '$from' and checkdate < '$to'";
			$r = $this->db->query($sql)->fetch();
			$sum = $r['taxes'];
            $idx = $j + 1;
            $ret['liab' . $idx] = $sum; 
        }

        return $ret;
	}

    function get_uct6_data($year, $qtr)
    {
		// Get rates and limits for SUTA
        $sql = "SELECT hilimit, rate FROM taxes WHERE taxtype = 'SUTA' AND year = $year";
        $a = $this->db->query($sql)->fetch();
        $suta_limit = $a['hilimit'];
        $suta_rate = $a['rate'];

		// Get cut-off dates

		// first month of quarter
		$first_month = $qtr + ($qtr - 1) * 2;

		// get from and to dates
        $dt = pdate::fromints($year, $first_month, 1);
        $dfrom = pdate::day_before_quarter($dt);
        $from = pdate::toiso($dfrom);
        $dto = pdate::day_after_quarter($dt);
        $to = pdate::toiso($dto);
		$db4year = pdate::day_before_year($dt);
		$b4year = pdate::toiso($db4year);

		// Get employees who were issued checks
        $sql = "SELECT DISTINCT paychecks.empid, empname, ssno FROM paychecks, employees WHERE checkdate > '$from' AND checkdate < '$to' AND employees.empid = paychecks.empid";
        $recs = $this->db->query($sql)->fetch_all();
		$emps = [];
		foreach ($recs as $a) {
            $emps[] = ['empid' => $a['empid'], 'empname' => $a['empname'], 'ssno' => $a['ssno'], 'gross' => 0.00, 'nontaxable' => 0.00, 'taxable' => 0.00];
        }
		$maxemps = count($emps);

		// Set up aggregate totals
		$aggregate_gross = 0.00;
		$aggregate_nontaxable = 0.00;
		$aggregate_taxable = 0.00;
		$aggregate_tax = 0.00;

        for ($i = 0; $i < $maxemps; $i++) {
            $empid = $emps[$i]['empid'];

			// Get before quarter figures for this employee
            $sql = "SELECT sum(gross) as gross FROM paychecks WHERE empid = '$empid' AND checkdate > '$b4year' AND checkdate <= '$from'";
            $b4 = $this->db->query($sql)->fetch()['gross'];
			if (is_null($b4)) {
				$b4 = 0;
			}
			// Get this quarter figures for this employee
            $sql = "SELECT sum(gross) as gross FROM paychecks WHERE empid = '$empid' AND checkdate > '$from' AND checkdate < '$to'";
            $q = $this->db->query($sql)->fetch()['gross'];

			// Set employee gross for this quarter
            $emps[$i]['gross'] = $q;

			// Figure out taxable/non-taxable for this quarter, per employee
			if ($b4 >= $suta_limit) {
				// the SUTA limit was reached/exceeded before this quarter
				$emps[$i]['taxable'] = 0;
				$emps[$i]['nontaxable'] = $q;
			}
			else {
				// still some remaining SUTA taxable wages this quarter
				$remaining_taxable = $suta_limit - $b4;
				if ($q > $remaining_taxable) {
					$emps[$i]['taxable'] = $remaining_taxable;
					$emps[$i]['nontaxable'] = $q - $remaining_taxable;
				}
				else {
					$emps[$i]['taxable'] = $q;
					$emps[$i]['nontaxable'] = 0;
				}
			}

			// Add to aggregate figures
			$aggregate_gross += $emps[$i]['gross'];
			$aggregate_nontaxable += $emps[$i]['nontaxable'];
			// this isn't currently asked for, but we'll provide it in case
			$aggregate_taxable += $emps[$i]['taxable'];

        }

		// Figure aggregate tax
		$aggregate_tax = round($aggregate_taxable * $suta_rate, 2, PHP_ROUND_HALF_UP);

		// Add to $emps array
		$emps[] = array('empid' => 'TOTAL', 'gross' => $aggregate_gross, 'nontaxable' => $aggregate_nontaxable, 'taxable' => $aggregate_taxable, 'tax' => $aggregate_tax);
        return $emps;
    }

    // This incredibly complex routine is designed to give us the FUTA liability
    // for each quarter. This comes up on the Form 940 when the total FUTA taxes
    // exceed $500.00.
    // I'd like to find a less complex way to do this. The calculation is complex
    // enough as it is, but then I have to pick up each employee for the year,
    // accumulate their gross for each quarter and the cumulative gross for the
    // prior quarters, then gauge the amount of FUTA taxable income based on the
    // high and low limits of FUTA tax, then accumulate all the figures for each
    // quarter, then figure the tax. What a pain!
    function get_futa_liab($year)
    {
        $sql = "SELECT hilimit, lolimit, rate FROM taxes WHERE taxtype = 'FUTA'";
        $taxes = $this->db->query($sql)->fetch_all();

		$fdoy = pdate::fromints($year, 1, 1);
		$dfrom = pdate::day_before_year($fdoy);
        $from = pdate::toiso($dfrom);
        $dto = pdate::day_after_year($fdoy);
        $to = pdate::toiso($dto);

        $emps = [];
        $sql = "SELECT DISTINCT empid FROM paychecks WHERE checkdate > '$from' AND checkdate < '$to'";
		$rec = $this->db->query($sql)->fetch_all();
		$maxemps = count($rec);
		foreach ($rec as $a) {
			$emps[] = [
				'empid' => $a['empid'],
				'gross' => [0.00, 0.00, 0.00, 0.00], 
				'cgross' => [0.00, 0.00, 0.00, 0.00],
				'taxable' => [0.00, 0.00, 0.00, 0.00]
			];
		}

		// get boundary dates for each quarter
        $qtr = [];
        for ($i = 0; $i < 4; $i++) {

			$month1 = (3 * $i) + 1;
			$startdt = pdate::fromints($year, $month1, 1);
			$dfrom = pdate::day_before_quarter($startdt);
			$from = pdate::toiso($dfrom);
			$dto = pdate::day_after_quarter($startdt);
			$to = pdate::toiso($dto);

            $qtr[] = ['from' => $from, 'to' => $to];
        }

        for ($j = 0; $j < $maxemps; $j++) {
            $empid = $emps[$j]['empid'];
            for ($i = 0; $i < 4; $i++) {
                $from = $qtr[$i]['from'];
                $to = $qtr[$i]['to'];
                $sql = "SELECT sum(gross) as gross FROM paychecks WHERE empid = '$empid' AND checkdate > '$from' AND checkdate < '$to'";
				$emps[$j]['gross'][$i] = $this->db->query($sql)->fetch()['gross'];
                for ($k = 0; $k < $i; $k++) {
                    $emps[$j]['cgross'][$i] += $emps[$j]['gross'][$k];
                }
            }
        }

        for ($i = 0; $i < $maxemps; $i++) {
            for ($j = 0; $j < 3; $j++) {
                $emps[$i]['taxable'][$j] = $this->get_taxable($taxes['lolimit'], $taxes['hilimit'], $emps[$i]['cgross'][$j], $emps[$i]['gross'][$j]);
            }
        }

        $liab = array(0.00, 0.00, 0.00, 0.00);
        for ($i = 0; $i < 3; $i++) {
            for ($j = 0; $j < count($emps); $j++) {
                $liab[$i] += bcmul($emps[$j]['taxable'][$i], $taxes['rate'], 2);
            }
        }
        return $liab;
    }

    function get_940_data($year)
    {
        $sql = "SELECT hilimit, rate FROM taxes WHERE taxtype = 'FUTA' AND year = $year";
        $a = $this->db->query($sql)->fetch();
        $futa_limit = $a['hilimit'];
        $futa_rate = $a['rate'];

		$fdoy = pdate::fromints($year, 1, 1);
        $dfrom = pdate::day_before_year($fdoy);
        $from = pdate::toiso($dfrom);
        $dto = pdate::day_after_year($fdoy);
        $to = pdate::toiso($dto);

        $sql = "SELECT DISTINCT empid FROM paychecks WHERE checkdate > '$from' AND checkdate < '$to'";
        $recs = $this->db->query($sql)->fetch_all();
		$emps = [];
		foreach ($recs as $a) {
            $emps[] = ['empid' => $a['empid']];
        }
		$maxemps = count($emps);

        $form = ['gross' => 0.00, 'exempt' => 0.00, 'taxable' => 0.00, 'tax' => 0.00];
        for ($i = 0; $i < $maxemps; $i++) {
            $empid = $emps[$i]['empid'];
            $sql = "SELECT sum(gross) AS gross FROM paychecks WHERE empid = '$empid' AND checkdate > '$from' AND checkdate < '$to'";
            $p = $this->db->query($sql)->fetch();
			if (is_null($p) || $p === FALSE) {
				$ytd_gross = 0;
			}
			else {
				$ytd_gross = $p['gross'];
			}

            $exempt = $ytd_gross - $futa_limit;
            if ($exempt < 0) {
                $taxable = $ytd_gross;
                $exempt = 0.00;
            }
            else {
                $taxable = $futa_limit;
            }
            $futa_tax = bcmul($taxable, $futa_rate, 2);
            $form['gross'] += $ytd_gross;
            $form['exempt'] += $exempt;
            $form['taxable'] += $taxable;
            $form['tax'] += $futa_tax;
        }

        if ($form['tax'] >= 500) {
            $liab = $this->get_futa_liab($year);
            $form['liab1'] = $liab[0];
            $form['liab2'] = $liab[1];
            $form['liab3'] = $liab[2];
            $form['liab4'] = $liab[3];
        }

        return $form;
    }

    function get_w2_data($year)
    {
		$fdoy = pdate::fromints($year, 1, 1);
        $dfrom = pdate::day_before_year($fdoy);
        $from = pdate::toiso($dfrom);
        $dto = pdate::day_after_year($fdoy);
        $to = pdate::toiso($dto);

        $sql = "SELECT DISTINCT empid FROM paychecks WHERE checkdate > '$from' AND checkdate < '$to'";
        $recs = $this->db->query($sql)->fetch_all();
		$emps = [];
		foreach ($recs as $a) {
            $emps[] = ['empid' => $a['empid']];
        }
		$maxemps = count($emps);

        $form = [];
        for ($i = 0; $i < $maxemps; $i++) {
            $empid = $emps[$i]['empid'];

            $sql = "SELECT sum(gross) as gross, sum(fwt + xfwt) as fwt, sum(essec + rssec) as ssec, sum(emedi + rmedi) as medi FROM paychecks WHERE empid = '$empid' AND checkdate > '$from' AND checkdate < '$to'";
            $paychecks = $this->db->query($sql)->fetch();

            $sql = "SELECT empid, empname, ssno FROM employees WHERE empid = '$empid'";
            $employees = $this->db->query($sql)->fetch();
            $form[] = array('empid' => $empid, 'empname' => $employees['empname'], 'ssno' => $employees['ssno'],
                        'gross' => $paychecks['gross'], 'fwt' => $paychecks['fwt'], 'ssec' => $paychecks['ssec'], 'medi' => $paychecks['medi']);
        }
        return $form;
    }

    function get_w3_data($year)
    {
        $form = ['nemps' => 0, 'gross' => 0.00, 'fwt' => 0.00, 'ssec' => 0.00, 'medi' => 0.00];
        $fw2 = $this->get_w2_data($year);
        for ($i = 0; $i < count($fw2); $i++) {
            $form['gross'] = bcadd($form['gross'], $fw2[$i]['gross'], 2);
            $form['fwt'] = bcadd($form['fwt'], $fw2[$i]['fwt'], 2);
            $form['ssec'] = bcadd($form['ssec'], $fw2[$i]['ssec'], 2);
            $form['medi'] = bcadd($form['medi'], $fw2[$i]['medi'], 2);
            $form['nemps']++;
        }
        return $form;
    }

};
