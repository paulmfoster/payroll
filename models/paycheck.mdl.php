<?php

// FIXME: no separate table for deduction allowances

class paycheck
{
    public $db;

    function __construct($db)
    {
        $this->db = $db;
    }

	function delete_check($checkno)
	{
		$this->db->delete('paychecks', "checkno = $checkno");
	}

	/**
	 * Get totals for each taxtype for all employees or for the employee in question,
	 * from one date to another (typically YTD).
	 *
	 * @param string Employee ID
	 * @param date Today's date, ISO format
	 * @return array Sum of each tax to date
	 */

    function get_ytd_totals($empid, $today)
    {
		$dtoday = pdate::fromiso($today);
		$db4year = pdate::day_before_year($dtoday);
		$b4year = pdate::toiso($db4year);

    	$sql = "select sum(futa) as futa, sum(suta) as suta, sum(emedi) as emedi, sum(rmedi) as rmedi, sum(essec) as essec, sum(rssec) as rssec, sum(gross) as gross, sum(fwt) as fwt, sum(xfwt) as xfwt, sum(net) as net from paychecks where empid = '$empid' and checkdate > '$b4year' and checkdate < '$today'";
        $arr = $this->db->query($sql)->fetch();

		// if there are no YTD records, the fields return as nulls, which messes
		// with our calculations
		$y = [];
		foreach ($arr as $key => $value) {
			$y[$key] = $value ?? 0;
		}

		return $y; 
	}

	function get_tax_tables_by_type($year)
	{
        $sql = "SELECT * FROM taxes WHERE year = $year ORDER BY taxtype, mstatus, base, lolimit";
        $tables = $this->db->query($sql)->fetch_all();

		$tbl = [];

		foreach ($tables as $table) {
			$taxtype = $table['whopays'] . $table['taxtype'];
			$tbl[$taxtype][] = $table;
		}

		return $tbl;
	}

	function get_medi_tax($taxes, $ytd, $wage)
	{
		$mtax = 0;
		$taxable = 0;
		foreach ($taxes as $tax) {
			if ($tax['hilimit'] == 0) {
				$tax['hilimit'] = PHP_FLOAT_MAX;
			}

			if ($ytd + $wage <= $tax['lolimit']) {
				// no tax owed
				$taxable = 0;
			}
			elseif ($ytd < $tax['lolimit'] && ($ytd + $wage) > $tax['lolimit']) {
				// the part of wage above lolimit is taxable
				$taxable = $ytd + $wage - $tax['lolimit'];
			}
			elseif ($ytd >= $tax['lolimit']) {
				if ($ytd + $wage <= $tax['hilimit']) {
					// all of wage is taxable
					$taxable = $wage;
				}
				elseif ($ytd < $tax['hilimit'] && ($ytd + $wage) > $tax['hilimit']) {
					// part of wage below hilimit taxable
					// leftover may be taxable in next bracket
					$taxable = $tax['hilimit'] - $ytd;
				}
			}
		
			$mtax += round($taxable * $tax['rate'], 2, PHP_ROUND_HALF_UP);
		}

		return $mtax;
	}

	function get_ssec_tax($taxes, $ytd, $wage)
	{
		$stax = 0;
		foreach ($taxes as $tax) {
			if ($tax['hilimit'] == 0) {
				$tax['hilimit'] = PHP_FLOAT_MAX;
			}

			if ($ytd < $tax['hilimit']) {
				if ($ytd + $wage <= $tax['hilimit']) {
					$taxable = $wage;
				}
				else {
					$taxable = $tax['hilimit'] - $ytd;
					$ytd = $tax['hilimit'];
				}
				$stax += round($taxable * $tax['rate'], 2, PHP_ROUND_HALF_UP);
			}
		}
		return $stax;
	}

	function get_fwt_tax($taxes, $yearly_gross, $mstatus, $periods)
	{
		$ftax = 0;
		foreach ($taxes as $p) {
			if ($p['hilimit'] == 0) {
				$p['hilimit'] = PHP_FLOAT_MAX;
			}

			if ($yearly_gross >= $p['lolimit'] && $yearly_gross < $p['hilimit'] && $mstatus == $p['mstatus']) {
				$ftax = round(($p['base'] + ($p['rate'] * ($yearly_gross - $p['lolimit']))) / $periods, PHP_ROUND_HALF_UP);
			}
		}
		return $ftax;
	}

	function get_suta_tax($taxes, $ytd, $wage)
	{
		$stax = 0;
		foreach ($taxes as $tax) {
			if ($tax['hilimit'] == 0) {
				$tax['hilimit'] = PHP_FLOAT_MAX;
			}

			if ($ytd < $tax['hilimit']) {
				if ($ytd + $wage <= $tax['hilimit']) {
					$taxable = $wage;
				}
				else {
					$taxable = $tax['hilimit'] - $ytd;
				}
				$stax += round($taxable * $tax['rate'], 2, PHP_ROUND_HALF_UP);
			}
		}
		return $stax;
	}

	function get_futa_tax($taxes, $ytd, $wage)
	{
		$ftax = 0;
		foreach ($taxes as $tax) {
			if ($tax['hilimit'] == 0) {
				$tax['hilimit'] = PHP_FLOAT_MAX;
			}

			if ($ytd < $tax['hilimit']) {
				if ($ytd + $wage <= $tax['hilimit']) {
					$taxable = $wage;
				}
				else {
					$taxable = $tax['hilimit'] - $ytd;
				}
				$ftax += round($taxable * $tax['rate'], 2, PHP_ROUND_HALF_UP);
			}
		}
		return $ftax;
	}

	/**
	 * Generate all data for a paycheck.
	 *
	 * This routine returns some information about the employee.
	 * It also returns all YTD gross, net and tax amounts.
	 * It also returns gross and tax amounts for the
	 * current pay period.
	 *
	 * @param string Employee ID
	 * @param integer Quantity of hours, weeks, etc.
	 * @return array All needed data to print checks
	 */

	function generate_paycheck_data($empid, $qty)
	{
		// fixme hard coded!
		$ded_rate = 4300;

		// the return variable
		$r = [];
		$r['qty'] = $qty;

		$dtoday = pdate::now();
		$year = $dtoday['y'];
		$today = pdate::toiso($dtoday);

		$sql = "SELECT * FROM employees WHERE empid = '$empid'";
		$emp = $this->db->query($sql)->fetch();

		$r['empid'] = $emp['empid'];
		$r['empname'] = $emp['firstname'] . ' ' . $emp['middleinit'] . ' ' . $emp['lastname'];
		$r['period'] = $emp['period'];
		$r['wage'] = $emp['wage'];

        $r['ytd'] = $this->get_ytd_totals($empid, $today);

		// figure projected yearly gross
		// this allows us to determine the tax bracket
		// this is only for federal withholding
		switch ($emp['period']) {
		case 'H':
			// 260 days * 8 hrs/day
			$periods = 52; // fixme assumes weekly pay
			$gross = $emp['wage'] * 2080;
			break;
		case 'D':
			// 52 wks * 5 days/wk
			$periods = 260;
			$gross = $emp['wage'] * $periods;
			break;
		case 'W':
			$periods = 52;
			$gross = $emp['wage'] * $periods;
			break;
		case 'B':
			$periods = 26;
			$gross = $emp['wage'] * $periods;
			break;
		case 'M':
			$periods = 12;
			$gross = $emp['wage'] * $periods;
			break;
		case 'Q':
			$periods = 4;
			$gross = $emp['wage'] * $periods;
			break;
		case 'Y':
			$periods = 1;
			$gross = $emp['wage'];
			break;
		}

		$r['gross'] = $qty * $emp['wage'];

		// needed for federal withholding
        $yearly_gross = $gross - ($emp['deds'] * $ded_rate);
		
		// get tax tables for this year
		$tbls = $this->get_tax_tables_by_type($year);

		// figure taxes
		$r['FWT'] = $this->get_fwt_tax($tbls['EFWT'], $yearly_gross, $emp['mstatus'], $periods);
		$r['XFWT'] = $emp['xfwt'];
		$r['EMEDI'] = $this->get_medi_tax($tbls['EMEDI'], $r['ytd']['gross'], $r['gross']);
		$r['RMEDI'] = $this->get_medi_tax($tbls['RMEDI'], $r['ytd']['gross'], $r['gross']);
		$r['RSSEC'] = $this->get_ssec_tax($tbls['RSSEC'], $r['ytd']['gross'], $r['gross']);
		$r['ESSEC'] = $this->get_ssec_tax($tbls['ESSEC'], $r['ytd']['gross'], $r['gross']);
		$r['SUTA'] = $this->get_suta_tax($tbls['RSUTA'], $r['ytd']['gross'], $r['gross']);
		$r['FUTA'] = $this->get_futa_tax($tbls['RFUTA'], $r['ytd']['gross'], $r['gross']);

		return $r;

	}

	function get_layout()
	{
        $sql = "SELECT * FROM cklayout ORDER BY line, col";
        $recs = $this->db->query($sql)->fetch_all();
		return $recs;
	}

	/**
	 * Get coordinates from the cklayout table for items appearing on the check.
	 *
	 * Units are in points (1/72 inch). Vertically 12 pt text = 1/6 inch. One line = 12 pt.
	 * Horizontally 12 pt text measures 1/10 inch. One char = 7.2 pt.
	 *
	 * @return array Coordinates of points on the check
	 */

	function get_coords()
   	{
        $xy = array();

        $sql = "SELECT * FROM cklayout";
        $recs = $this->db->query($sql)->fetch_all();

		foreach ($recs as $arr) {
            switch ($arr['code']) {
                case 'CKLINES':
                    $xy['cklines'] = $arr['line'];
                    break;
                case 'CKNUM':
					$xy['cknum']['x'] = $arr['col'];
                    $xy['cknum']['y'] = $arr['line'];
                    break;
                case 'AMTSPELLED':
                    $xy['amtspelled']['x'] = $arr['col'];
                    $xy['amtspelled']['y'] = $arr['line'];
                    break;
                case 'CKDATE':
                    $xy['ckdate']['x'] = $arr['col'];
                    $xy['ckdate']['y'] = $arr['line'];
                    break;
                case 'CKAMT':
                    $xy['ckamt']['x'] = $arr['col'];
                    $xy['ckamt']['y'] = $arr['line'];
                    break;
                case 'VENDOR':
                    $xy['vendor']['x'] = $arr['col'];
                    $xy['vendor']['y'] = $arr['line'];
                    break;
            }
        }

        return $xy;
    }

	function update_coords($post)
	{
		foreach ($post as $key => $val) {
			if (strpos($key, 'x_') === 0) {
				// process col
				$which = substr($key, 2);
				$this->db->update('cklayout', ['col' => $val], "code = '$which'");
			}
			elseif (strpos($key, 'y_') === 0) {
				// process line
				$which = substr($key, 2);
				$this->db->update('cklayout', ['line' => $val], "code = '$which'");
			}
		}
	}

    /**
	 * Process each thousands group for spelled amounts.
	 *
	 * @param string The buffer we work in.
	 * @param float The amount in question.
	 * @param string The suffix to use
	 * @return string The changed buffer
     */

    function form_group($buf, $amt, $scale)
    {
		$units = array("Zero", "One", "Two", "Three", "Four",
			"Five", "Six", "Seven", "Eight", "Nine",
			"Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
			"Fifteen", "Sixteen", "Seventeen", "Eighteen",
			"Nineteen");

		$tens = array("Twenty", "Thirty", "Forty", "Fifty", "Sixty",
			"Seventy", "Eighty", "Ninety");

        if ($buf != '')
            $buf .= ' ';

        if (100 <= $amt) {
            $buf .= $units[$amt/100] . ' Hundred ';
            $amt %= 100;

        }

        if (20 <= $amt) {
            $buf .= $tens[($amt - 20)/10];
            if (0 != ($amt %= 10)) {
                $buf .= '-' . $units[$amt] . ' ';
            }
            else
                $buf .= ' ';
        }
        elseif ($amt) {
            $buf .= $units[$amt] . ' ';
        }

        $buf .= $scale;
        return $buf;
    }

	/**
	 * Spell out the amount passed in.
	 * This will round to 2 places.
	 *
	 * @param float Number to spell
	 * @return string The spelled out number
     */

    function spell_amount($number)
    {

        $arr = explode('.', $number);
        $dollars = $arr[0];
        $cents = $arr[1];

        // This is a fix to prevent XXXX.8 from being interpreted as XXXX.08
        if ($cents < 10 and $cents != 0)
            if (strlen($cents) < 2)
                $cents .= '0';

        $buf = '';

        $amt = $dollars;

        $temp = (int)($amt/1E12);
        if ($temp) {
            $buf = $this->form_group($buf, $temp, "Trillion");
            $amt -= $temp * 1E12;
        }

        $temp = (int)($amt/1E9);
        if ($temp) {
            $buf = $this->form_group($buf, $temp, "Billion");
            $amt -= $temp * 1E9;
        }

        $temp = (int)($amt/1E6);
        if ($temp) {
            $buf = $this->form_group($buf, $temp, "Million");
            $amt -= $temp * 1E6;
        }

        $temp = (int)($amt/1E3);
        if ($temp) {
            $buf = $this->form_group($buf, $temp, "Thousand");
            $amt -= $temp * 1E3;
        }

        $buf = $this->form_group($buf, $amt, "");

        if ($buf == '')
            $buf .= 'Zero' . ' ';

        $temp = (int) $cents;
        $buf .= sprintf("And %02d/100", $temp);

        return $buf;
    }

	/**
	 * Generate paychecks as directed by controller.
	 *
	 * The POST array should contain:
	 * - checkno Check number
	 * - startdt Start date of this pay period
	 * - enddt End date of this pay period
	 * - [qty_empid]... Qty of pay units per employee
	 * 
	 * This routine will generate all the YTD amounts,
	 * create PDF checks, and store the data.
	 *
	 * @param array The POST array
	 */

	function generate_paychecks($post)
	{
		global $cfg;
		include $cfg['libdir'] . 'pdf_report.lib.php';
		include $cfg['reportdir'] . 'paycheck_report.rpt.php';

		// coordinates for fields on the checks
		$xy = $this->get_coords();

		$c = [];
		$c['checkno'] = (int) $post['checkno'];
		$c['startdt'] = $post['startdt'];
		$c['enddt'] = $post['enddt'];
		$c['checkdate'] = pdate::now2iso();

		foreach ($post as $key => $qty) {
			if (empty($qty)) {
				continue;
			}
			if (strpos($key, 'qty_') === 0) {
				$empid = substr($key, 4);
				$ck = $this->generate_paycheck_data($empid, $qty);

				$c['empid'] = $ck['empid'];
				$c['empname'] = $ck['empname'];
				$c['period'] = $ck['period'];
				$c['wage'] = $ck['wage'];
				$c['ytd'] = $ck['ytd'];
				$c['qty'] = $ck['qty'];
				$c['gross'] = $ck['gross'];
				$c['fwt'] = $ck['FWT'] + $ck['XFWT'];
				$c['medi'] = $ck['EMEDI'];
				$c['ssec'] = $ck['ESSEC'];
				$c['net'] = $c['gross'] - $c['fwt'] - $c['medi'] - $c['ssec']; 
				$c['ytdgross'] = $ck['ytd']['gross'];
				$c['ytdfwt'] = $ck['ytd']['fwt'] + $ck['ytd']['xfwt'];
				$c['ytdssec'] = $ck['ytd']['essec'];
				$c['ytdmedi'] = $ck['ytd']['emedi'];
				$c['ytdnet'] = $ck['ytd']['net'];

				$c['amtspelled'] = $this->spell_amount($c['net']);

				// generate the PDF check
				$filename = $cfg['printdir'] . 'check-'. $c['checkno'] . '.pdf';
				$pr = new paycheck_report;
				$pr->check($xy, $c, $filename);
				
				$k = [
					'empid' => $c['empid'],
					'checkdate' => $c['checkdate'],
					'checkno' => $c['checkno'],
					'gross' => $c['gross'],
					'net' => $c['net'],
					'fwt' => $ck['FWT'],
					'xfwt' => $ck['XFWT'],
					'essec' => $ck['ESSEC'],
					'rssec' => $ck['RSSEC'],
					'emedi' => $ck['EMEDI'],
					'rmedi' => $ck['RMEDI'],
					'suta' => $ck['SUTA'],
					'futa' => $ck['FUTA']
				];
    			$this->save_paycheck_data($k);

				$c['checkno']++;
			}
		}
	}


	/**
	 * Save data for a single paycheck to the database.
	 */

    function save_paycheck_data($check)
	{
		$this->db->insert('paychecks', $check);
    }

    function get_employees()
    {
        $sql = "SELECT * FROM employees ORDER BY empid";
        $arr = $this->db->query($sql)->fetch_all();
        return $arr;
    }

};
