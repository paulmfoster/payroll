
<h3>IRS Form W3 for Year <?php echo $_POST['year']; ?></h3>
<table>
<th>Explanation</th><th>Value</th>
<tr>
    <td>Kind of payer</td>
    <td align="right">941</td>
<tr>
<tr>
    <td>Number of W2s</td>
    <td align="right"><?php echo $fw3['nemps']; ?></td>
<tr>

<!-- FIXME: hardcoded company data -->

<tr>
    <td>EIN Number</td>
    <td align="right">59-3383045</td>
</tr>
<tr>
    <td>Company and Address</td>
    <td>Quill &amp; Mouse Studios, Inc.</td>
</tr>
<tr>
    <td></td>
    <td>1901 N. Highland Ave.</td>
</tr>
<tr>
    <td></td>
    <td>Clearwater, FL 33755</td>
</tr>

<tr>
    <td>Gross Wages</td>
    <td align="right"><?php echo $fw3['gross']; ?></td>
</tr>
<tr>
    <td>Federal Withholding</td>
    <td align="right"><?php echo $fw3['fwt']; ?></td>
</tr>
<tr>
    <td>Social Security Wages</td>
    <td align="right"><?php echo $fw3['gross']; ?></td>
</tr>
<tr>
    <td>Social Security Taxes</td>
    <td align="right"><?php echo $fw3['ssec']; ?></td>
</tr>
<tr>
    <td>Medicare Wages</td>
    <td align="right"><?php echo $fw3['gross']; ?></td>
</tr>
<tr>
    <td>Medicare Taxes</td>
    <td align="right"><?php echo $fw3['medi']; ?></td>
</tr>

<!-- FIXME: hardcoded contact person -->

<tr>
    <td>Contact Person</td>
    <td>Paul M. Foster</td>
</tr>
<tr>
    <td>Telephone Number</td>
    <td>727-442-8487</td>
</tr>
<tr>
    <td>Fax Number</td>
    <td>727-442-4412</td>
</tr>

</table>
<p>
Click <a href="<?php echo $cfg['printdir'] . 'formw3.pdf'; ?>" target="_blank"><img src="<?php echo $cfg['imgdir']; ?>pdf-48x48.png"/></a> to print the report.
