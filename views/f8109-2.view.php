
<table>
<tr><td><strong>Gross</strong></td><td align="right"><?php echo number_format($data['gross'], 2); ?></td></tr>
<tr><td><strong>Federal Withholding</strong></td><td align="right"><?php echo number_format($data['fwt'], 2); ?></td></tr>
<tr><td><strong>Extra Withholding</strong></td><td align="right"><?php echo number_format($data['xfwt'], 2); ?></td></tr>
<tr><td><strong>Total Withholding</strong></td><td align="right"><?php echo number_format($data['fwt'] + $data['xfwt'], 2); ?></td></tr>
<tr><td><strong>Employee Social Security</strong></td><td align="right"><?php echo number_format($data['essec'], 2); ?></td></tr>
<tr><td><strong>Employer Social Security</strong></td><td align="right"><?php echo number_format($data['rssec'], 2); ?></td></tr>
<tr><td><strong>Employee Medicare</strong></td><td align="right"><?php echo number_format($data['emedi'], 2); ?></td></tr>
<tr><td><strong>Employer Medicare</strong></td><td align="right"><?php echo number_format($data['rmedi'], 2); ?></td></tr>
<tr><td><strong>State Unemployment</strong></td><td align="right"><?php echo number_format($data['suta'], 2); ?></td></tr>
<tr><td><strong>Federal Unemployment</strong></td><td align="right"><?php echo number_format($data['futa'], 2); ?></td></tr>
<tr><td><strong>Net</strong></td><td align="right"><?php echo number_format($data['net'], 2); ?></td></tr>
<tr><td><strong>941 Taxes Deposit</strong></td><td align="right"><?php echo number_format($data['taxes'], 2); ?></td></tr>
</table>
<p>
<strong>Click <a href="<?php echo $cfg['printdir'] . 'form8109.pdf'; ?>" target="_blank"><img src="<?php echo $cfg['imgdir']; ?>pdf-48x48.png"/></a> for printed report.</strong>
