
<table>
<tr>
<th></th>
<th>Year</th>
<th>Tax Type</th>
<th>Filing Status</th>
<th>Who Pays</th>
<th>Low Limit</th>
<th>High Limit</th>
<th>Base</th>
<th>Rate %</th>
</tr>

<?php foreach ($taxes as $tax): ?>
<tr>
<td>
<a href="tbledt.php?id=<?php echo $tax['id']; ?>">Edit</a>
/
<a href="tbldel.php?id=<?php echo $tax['id']; ?>">Delete</a>
</td>
<td><?php echo $tax['year']; ?></td>
<td><?php echo $tax['taxtype']; ?></td>
<td><?php echo $tax['mstatus']; ?></td>
<td><?php echo $tax['whopays']; ?></td>
<td><?php echo $tax['lolimit']; ?></td>
<td><?php echo $tax['hilimit']; ?></td>
<td><?php echo $tax['base']; ?></td>
<td><?php echo $tax['rate'] * 100; ?></td>
</tr>
<?php endforeach; ?>

</table>
<br/>
<?php form::button('Add New Tax Table Row', 'tbladd.php'); ?>

