<h3>IRS Form 940 for Year <?php echo $_POST['year']; ?></h3>
<p>
<table>
<th>Explanation</th><th>Value</th>
<!-- FIXME: hardcoded state data -->
<tr>
    <td>State</td>
    <td align="right">FL</td>
</tr>
<tr>
    <td>State reporting number</td>
    <td align="right">1577436-3</td>
</tr>
<tr>
    <td>Total payments to all employees</td>
    <td align="right"><?php echo number_format($f940['gross'], 2); ?></td>
</tr>
<tr>
    <td>Total payments in excess of FUTA limit per employee</td>
    <td align="right"><?php echo number_format($f940['exempt'], 2); ?></td>
</tr>
<tr>
    <td>Total taxable FUTA wages</td>
    <td align="right"><?php echo number_format($f940['taxable'], 2); ?></td>
</tr>
<tr>
    <td>FUTA Tax (calculated)</td>
    <td align="right"><?php echo number_format($f940['tax'], 2); ?></td>
</tr>
<?php if ($f940['tax'] > 500): ?>
    <tr>
        <td>1st Qtr FUTA Liability</td>
        <td><?php echo number_format($f940['liab1'], 2); ?></td>
    </tr>
    <tr>
        <td>2nd Qtr FUTA Liability</td>
        <td><?php echo number_format($f940['liab2'], 2); ?></td>
    </tr>
    <tr>
        <td>3rd Qtr FUTA Liability</td>
        <td><?php echo number_format($f940['liab3'], 2); ?></td>
    </tr>
    <tr>
        <td>4th Qtr FUTA Liability</td>
        <td><?php echo number_format($f940['liab4'], 2); ?></td>
    </tr>
    <tr>
        <td>Total FUTA Liability</td>
        <td><?php echo number_format($f940['tax'], 2); ?></td>
    </tr>
<?php endif; ?>
</table>
<p>
Click <a href="<?php echo $cfg['printdir'] . 'form940.pdf'; ?>" target="_blank"><img src="<?php echo $cfg['imgdir']; ?>pdf-48x48.png"/></a> to print the report.
