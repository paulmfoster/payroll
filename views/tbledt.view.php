<form method="post" action="<?php echo $return; ?>">
<?php $form->hidden('id'); ?>
<table>
<tr>
	<td>Tax Year</td>
	<td><?php $form->text('year'); ?></td>
</tr>
<tr>
	<td>Tax Type</td>
	<td><?php $form->select('taxtype'); ?></td>
</tr>
<tr>
	<td>Who Pays</td>
	<td><?php $form->select('whopays'); ?></td>
</tr>
<tr>
	<td>Filing Status</td>
	<td><?php $form->select('mstatus'); ?></td>
</tr>
<tr>
	<td>Wages at least:</td>
	<td><?php $form->text('lolimit'); ?></td>
</tr>
<tr>
	<td>But less than:</td>
	<td><?php $form->text('hilimit'); ?></td>
</tr>
<tr>
	<td>Tentative amount of tax:</td>
	<td><?php $form->text('base'); ?></td>
</tr>
<tr>
	<td>Plus this % of amt over base:</td>
	<td><?php $form->text('rate'); ?></td>
</tr>
<tr>
	<td colspan="2"><?php $form->submit('s1'); ?>
</tr>
</table>
</form>

