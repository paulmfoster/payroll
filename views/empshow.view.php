<table>
<tr>
<td class="tdlabel">Employee ID</td>
<td><?php echo $emp['empid']; ?></td>
</tr>
<tr>
<td class="tdlabel">First Name</td>
<td><?php echo $emp['firstname']; ?></td>
</tr>
<tr>
<td class="tdlabel">Middle Initial</td>
<td><?php echo $emp['middleinit']; ?></td>
</tr>
<tr>
<td class="tdlabel">Last Name</td>
<td><?php echo $emp['lastname']; ?></td>
</tr>
<tr>
<td class="tdlabel">Email</td>
<td><?php echo $emp['email']; ?></td>
</tr>
<tr>
<td class="tdlabel">Soc Sec #</td>
<td><?php echo $emp['ssno']; ?></td>
</tr>
<tr>
<td class="tdlabel">Filing Status</td>
<td><?php echo $emp['mstatus']; ?></td>
</tr>
<tr>
<td class="tdlabel">Deductions</td>
<td><?php echo $emp['deds']; ?></td>
</tr>
<tr>
<td class="tdlabel">Extra FWT</td>
<td><?php echo $emp['xfwt']; ?></td>
</tr>
<tr>
<td class="tdlabel">Wage</td>
<td><?php echo $emp['wage']; ?></td>
</tr>
<tr>
<td class="tdlabel">Period</td>
<td><?php echo $emp['period']; ?></td>
</tr>
</table>
<p>
<?php form::button('Edit', 'empedt.php?empid=' . $emp['empid']); ?>
&nbsp;
<?php form::button('Delete', 'empdel.php?empid=' . $emp['empid']); ?>

