<div style="width: 50em">
This screen allows you to modify where things are printed on checks.
It is set up for letter size checks printed with a stub on the top,
the check in the middle, and a second stub on the bottom. You may change
this by changing coordinates.
<p>
Coordinates are human understandable units. Vertically, a line is roughly
1/6th of an inch high. So there are 66 lines to a page. Horizontally, 
characters take up about 1/10th of an inch. At print time, these are
converted to a printer measure called "points" (pt). The software which
produces the PDF needs things measured this way, so we do the conversion
internally.

</div>

<form method="post" action="<?php echo $return; ?>">

<table>

<tr><th>Column</th><th>Line</th><th>Code</th><th>Description</th></tr>

<?php foreach ($xy as $rec): ?>

<tr>
<td><input type="text" size="3" maxlength="3" name="<?php echo "x_{$rec['code']}"; ?>" value="<?php echo $rec['col']; ?>"/></td>
<td><input type="text" size="3" maxlength="3" name="<?php echo "y_{$rec['code']}"; ?>" value="<?php echo $rec['line']; ?>"/></td>
<td><?php echo $rec['code']; ?></td>
<td><?php echo $rec['descrip']; ?></td>
</tr>

<?php endforeach; ?>

</table>

&nbsp;
<input type="submit" name="s1" value="Save">

</form>
