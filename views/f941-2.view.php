<h3>IRS Form 941 for Quarter <?php echo $_POST['quarter']; ?>, Year <?php echo $_POST['year']; ?></h3>
<table>
<th>Explanation</th><th>Value</th>
<tr>
    <td>Number of employees on 12th day of quarter</td>
    <td align="right"><?php echo $f941['nemps']; ?></td>
</tr>
<tr>
    <td>Gross wages</td>
    <td align="right"><?php echo number_format($f941['gross'], 2); ?></td>
</tr>
<tr>
    <td>Fed W/H tax</td>
    <td align="right"><?php echo number_format($f941['fwt'], 2); ?></td>
</tr>
<tr>
    <td>Soc Sec taxes (calculated)</td>
    <td align="right"><?php echo number_format($f941['ssec'], 2); ?></td>
</tr>
<tr>
    <td>Medicare taxes (calculated)</td>
    <td align="right"><?php echo number_format($f941['medi'], 2); ?></td>
</tr>
<tr>
    <td>Soc Sec + Medicare taxes (calculated)</td>
    <td align="right"><?php echo number_format($f941['ssec_medi'], 2); ?></td>
</tr>
<tr>
    <td>Total taxes (calculated)</td>
    <td align="right"><?php echo number_format($f941['total_taxes'], 2); ?></td>
</tr>
<tr>
    <td>Fractions of Cents adjustment</td>
    <td align="right"><?php echo number_format($f941['cent_adj'], 2); ?></td>
</tr>
<tr>
    <td>Taxes plus/minus adjustments</td>
    <td align="right"><?php echo number_format($f941['collected_taxes'], 2); ?></td>
</tr>
<tr>
    <td>Deposits this quarter</td>
    <td align="right"><?php echo number_format($f941['collected_taxes'], 2); ?></td>
</tr>
<tr>
    <td>Balance due</td>
    <td align="right">0.00</td>
</tr>
<tr>
    <td>This state</td>
    <td align="right">FL</td>
</tr>
<tr>
    <td>First month liability</td>
    <td align="right"><?php echo number_format($f941['liab1'], 2); ?></td>
</tr>
<tr>
    <td>Second month liability</td>
    <td align="right"><?php echo number_format($f941['liab2'], 2); ?></td>
</tr>
<tr>
    <td>Third month liability</td>
    <td align="right"><?php echo number_format($f941['liab3'], 2); ?></td>
</tr>
<tr>
    <td>Total liability</td>
    <td align="right"><?php echo number_format($f941['collected_taxes'], 2); ?></td>
</tr>
</table>
<p>
Click <a href="<?php echo $cfg['printdir'] . 'form941.pdf'; ?>" target="_blank"><img src="<?php echo $cfg['imgdir']; ?>pdf-48x48.png"/></a> for printed report.
<p>
