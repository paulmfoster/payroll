
<h3>IRS Forms W2 for Year <?php echo $_POST['year']; ?></h3>
<?php for ($i = 0; $i < count($fw2); $i++): ?>
<table>
	<th>Explanation</th><th>Value</th>
	<tr>
        <td>FEI Number</td>
        <td align="right">59-3383045</td>
    </tr>
	<tr>
        <td>Company and Address</td>
		<td><?php echo $cfg['company_name']; ?></td>
    </tr>
	<tr>
        <td></td>
		<td><?php echo $cfg['address']; ?></td>
    </tr>
	<tr>
        <td></td>
		<td><?php echo $cfg['city_state_zip']; ?></td>
    </tr>
	<tr>
        <td>Employee SS#</td>
        <td align="right"><?php echo $fw2[$i]['ssno']; ?></td>
    </tr>
	<tr>
        <td>Employee Name</td>
        <td align="right"><?php echo $fw2[$i]['empname']; ?></td>
    </tr>

	<!-- FIXME: should have employee address data -->

	<tr>
        <td>Gross Wages</td>
        <td align="right"><?php echo number_format($fw2[$i]['gross'], 2); ?></td>
    </tr>
	<tr>
        <td>Federal Withholding</td>
        <td align="right"><?php echo number_format($fw2[$i]['fwt'], 2); ?></td>
    </tr>
	<tr>
        <td>Social Security Wages</td>
        <td align="right"><?php echo number_format($fw2[$i]['gross'], 2); ?></td>
    </tr>
	<tr>
        <td>Social Security Taxes</td>
        <td align="right"><?php echo number_format($fw2[$i]['ssec'], 2); ?></td>
    </tr>
	<tr>
		<td>Medicare Wages</td>
        <td align="right"><?php echo number_format($fw2[$i]['gross'], 2); ?></td>
    </tr>
	<tr>
        <td>Medicare Taxes</td>
        <td align="right"><?php echo number_format($fw2[$i]['medi'], 2); ?></td>
    </tr>
</table>
<?php endfor; ?>
<p>
	Click <a href="<?php echo $cfg['printdir'] . 'formw2.pdf'; ?>"><img src="<?php echo $cfg['imgdir']; ?>pdf-48x48.png"/></a> to print the reports.
