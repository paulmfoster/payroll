<h3>Fill in a number in the "How Many" field for each check you wish to generate.<br/>
This is important for hourly employees (e.g. 40). No checks will be issued for<br/>
employees where this field is left blank.
</h3>
<form method="post" action="<?php echo $return; ?>">
<table>
<tr>
<th>How Many</th><th>Pay Period</th><th>Employee</th></tr>
<?php foreach ($emps as $emp): ?>
<tr>
<td>
<?php $form->text('qty_' . $emp['empid']); ?>
</td>
<td>
<?php
switch ($emp['period']) {
case 'H':
	$period = 'Hourly';
	break;
case 'D':
	$period = 'Daily';
	break;
case 'W':
	$period = 'Weekly';
	break;
case 'B':
	$period = 'Biweekly';
	break;
case 'M':
	$period = 'Monthly';
	break;
case 'Q':
	$period = 'Quarterly';
	break;
case 'Y':
	$period = 'Yearly';
	break;
}
echo $period;
?>
</td>
<td><?php echo $emp['empid'] . ' - ' . $emp['firstname'] . ' ' . $emp['middleinit'] . ' ' . $emp['lastname']; ?></td>
</tr>
<?php endforeach; ?>
</table>
<p>

<table>

<tr>
<td class="tdlabel">Starting check #</td>
<td><?php $form->text('checkno'); ?></td>
</tr>

<tr>
<td class="tdlabel">Start Date</td>
<td><?php $form->date('startdt'); ?></td>
</tr>

<tr>
<td class="tdlabel">End Date</td>
<td><?php $form->date('enddt'); ?></td>
</tr>

</table>
</p>

<p>
<?php $form->submit('s1'); ?>
</p>

</form>
