
<h3>Florida Form UCT-6 (Rev 03/12) for Quarter <?php echo $_POST['quarter']; ?>, Year <?php echo $_POST['year']; ?></h3>
<table>
<th>Explanation</th><th>Value</th>
<?php foreach ($fuct6 as $f): ?>
<?php if ($f['empid'] == 'TOTAL'): ?>
<tr>
    <td>Gross wages</td>
    <td align="right"><?php echo number_format($f['gross'], 2); ?></td>
</tr>
<tr>
    <td>Gross wages over SUTA limit/year per employee</td>
    <td align="right"><?php echo number_format($f['nontaxable'], 2); ?></td>
</tr>
<tr>
    <td>Taxable wages (Line #2 - Line #3)</td>
    <td align="right"><?php echo number_format($f['taxable'], 2); ?></td>
</tr>
<tr>
    <td>Tax Due</td>
    <td align="right"><?php echo number_format($f['tax'], 2); ?></td>
</tr>
<tr>
    <td>Total Tax Due</td>
    <td align="right"><?php echo number_format($f['tax'], 2); ?></td>
</tr>
<?php break; ?>
<?php endif; ?>
<?php endforeach; ?>

<?php foreach ($fuct6 as $f): ?>
<?php if ($f['empid'] != 'TOTAL'): ?>
	<tr>
        <td>SS #</td>
        <td align="right"><?php echo $f['ssno']; ?></td>
    </tr>
	<tr>
        <td>Employee Name</td>
        <td align="right"><?php echo $f['empname']; ?></td>
    </tr>
	<tr>
        <td>Gross Wages</td>
        <td align="right"><?php echo number_format($f['gross'], 2); ?></td>
    </tr>
	<tr>
        <td>Taxable</td>
        <td align="right"><?php echo number_format($f['taxable'], 2); ?></td>
    </tr>
<?php endif; ?>
<?php endforeach; ?>

<?php foreach ($fuct6 as $f): ?>
<?php if ($f['empid'] == 'TOTAL'): ?>
<tr>
    <td>Total Gross Wages</td>
    <td align="right"><?php echo number_format($f['gross'], 2); ?></td>
</tr>
<tr>
    <td>Total Taxable Wages</td>
    <td align="right"><?php echo number_format($f['taxable'], 2); ?></td>
</tr>
<?php break; ?>
<?php endif; ?>
<?php endforeach; ?>
</table>
<p>
Click <a href="<?php echo $cfg['printdir'] . 'formuct6.pdf'; ?>" target="_blank"><img src="<?php echo $cfg['imgdir']; ?>pdf-48x48.png"/></a> for printed report.
<p>
