<table>
<th>Delete</th>
<th>Check #</th>
<th>Date</th>
<th>Emp ID</th>
<th>Gross</th>
<th>FWT</th>
<th>XFWT</th>
<th>SSEC</th>
<th>MEDI</th>
<th>FUTA</th>
<th>SUTA</th>
<th>Net</th>
</tr>
<?php foreach ($checks as $ck): ?>
<tr>
<td><a href="delcheck.php?checkno=<?php echo $ck['checkno']; ?>">Delete</a></td>
<td><?php echo $ck['checkno']; ?></td>
<td><?php echo pdate::iso2am($ck['checkdate']); ?></td>
<td><?php echo $ck['empid']; ?></td>
<td><?php echo number_format($ck['gross'], 2, '.', ''); ?></td>
<td><?php echo number_format($ck['fwt'], 2, '.', ''); ?></td>
<td><?php echo number_format($ck['xfwt'], 2, '.', ''); ?></td>
<td><?php echo number_format($ck['essec'] + $ck['rssec'], 2, '.', ''); ?></td>
<td><?php echo number_format($ck['emedi'] + $ck['rmedi'], 2, '.', ''); ?></td>
<td><?php echo number_format($ck['futa'], 2, '.', ''); ?></td>
<td><?php echo number_format($ck['suta'], 2, '.', ''); ?></td>
<td><?php echo number_format($ck['net'], 2, '.', ''); ?></td>
</tr>
<?php endforeach; ?>
</table>

