<h3>Click the filename to print or the Delete link to delete.</h3>
<?php foreach ($files as $file): ?>

<strong>
<?php form::button('Delete', 'delfile.php?file=' . $file); ?>&nbsp;
<a href="<?php echo $cfg['printdir'] . $file; ?>" target="_blank"><?php echo $file; ?></a>
</strong><br/>

<?php endforeach; ?>
