<?php
include 'init.php';
include $cfg['grottodir'] . 'Parsedown.php';
$pd = new Parsedown();
$content = file_get_contents('README.md');
$text = '<div style="width: 50em">';
$text .= $pd->text($content);
$text .= '</div>';
view('Home', ['text' => $text], '', 'index');
