<?php

/**
 * @copyright 2021 Paul M. Foster <paulf@quillandmouse.com>
 * @author Paul M. Foster <paulf@quillandmouse.com>
 * @license GPL2
 */

$cfgfile = 'config/config.ini';
if (!file_exists($cfgfile)) {
	copy('config/config.sample', 'config/config.ini');
}
$cfg = parse_ini_file('config/config.ini');

include 'grotto_check.php';
include $cfg['grottodir'] . 'misc.inc.php';

// one month
ini_set('session.gc_maxlifetime', 2592000);
ini_set('session.cookie_lifetime', 2592000);
session_set_cookie_params(2592000);
session_name($cfg['session_name']);
session_start();

grotto('errors');
grotto('numbers');
grotto('messages');
grotto('pdate');
grotto('memory');
$form = grotto('form');
$nav = grotto('navigation');

include 'links.php';
$nav->init('A', $links);

$db = grotto('database', $cfg);
if (!$db->status()) {
	make_tables($db);
}

