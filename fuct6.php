<?php
include 'init.php';
$pr = model('payroll');

$quarter_options = [
	['lbl' => 'First', 'val' => 1],
	['lbl' => 'Second', 'val' => 2],
	['lbl' => 'Third', 'val' => 3],
	['lbl' => 'Fourth', 'val' => 4]
];

$years = $pr->get_years();
if ($years === FALSE) {
	emsg('F', 'No paychecks have been generated.');
	redirect('chkgen.php');
}

$year_options = [];
foreach ($years as $year) {
	$year_options[] = ['lbl' => $year, 'val' => $year];
}

$fields = [
	'quarter' => [
		'name' => 'quarter',
		'type' => 'select',
		'options' => $quarter_options,
		'label' => 'Quarter'
	],
	'year' => [
		'name' => 'year',
		'type' => 'select',
		'options' => $year_options,
		'label' => 'Year'
	],
	's1' => [
		'name' => 's1',
		'type' => 'submit',
		'value' => 'Report'
	]
];
$form->set($fields);

view('Form UCT6 (Quarterly)', [], 'fuct6-2.php', 'fuct6');

