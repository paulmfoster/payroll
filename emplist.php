<?php
include 'init.php';
$pr = model('payroll');

$emps = $pr->get_employees();
if ($emps === FALSE) {
	emsg('F', 'No employees found. Please create one.');
	redirect('empadd.php');
}

view('Employees List', ['emps' => $emps], '', 'emplist');

