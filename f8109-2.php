<?php
include 'init.php';
$s1 = fork('s1', 'P', 'index.php');
$pr = model('payroll');

// get data for report(s)
$data = $pr->get_8109_data($_POST['year'], $_POST['month']);
$months = $pr->get_months();

// PDF report
$pdfr = model('forms');
$pdfr->form8109($months[$_POST['month'] - 1], $_POST['year'], $data, $cfg['printdir'] . 'form8109.pdf');

// show it, baby!
view('Form 8109 Display', ['data' => $data], '', 'f8109-2');

