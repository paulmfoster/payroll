<?php
include 'init.php';

$mstatus_options = [
	['lbl' => 'Married/Joint', 'val' => 'M'],
	['lbl' => 'Single', 'val' => 'S'],
	['lbl' => 'Head of Household', 'val' => 'H']
];

$period_options = [
	['lbl' => 'Hourly', 'val' => 'H'],
	['lbl' => 'Daily', 'val' => 'D'],
	['lbl' => 'Weekly', 'val' => 'W'],
	['lbl' => 'Biweekly', 'val' => 'B'],
	['lbl' => 'Monthly', 'val' => 'M'],
	['lbl' => 'Quarterly', 'val' => 'Q'],
	['lbl' => 'Yearly', 'val' => 'Y']
];

$fields = [
	'empid' => [
		'name' => 'empid',
		'type' => 'text',
		'size' => 8,
		'maxlength' => 8,
		'label' => 'Employee ID',
		'required' => 1
	],
	'firstname' => [
		'name' => 'firstname',
		'type' => 'text',
		'size' => 35,
		'maxlength' => 35,
		'label' => 'First Name',
		'required' => 1
	],
	'middleinit' => [
		'name' => 'middleinit',
		'type' => 'text',
		'size' => 1,
		'maxlength' => 1,
		'label' => 'Middle Initial'
	],
	'lastname' => [
		'name' => 'lastname',
		'type' => 'text',
		'size' => 35,
		'maxlength' => 35,
		'label' => 'Last Name',
		'required' => 1
	],
	'email' => [
		'name' => 'email',
		'type' => 'text',
		'size' => 50,
		'maxlength' => 50,
		'label' => 'Email'
	],
	'ssno' => [
		'name' => 'ssno',
		'type' => 'text',
		'size' => 9,
		'maxlength' => 9,
		'label' => 'Social Security #',
		'required' => 1
	],
	'mstatus' => [
		'name' => 'mstatus',
		'type' => 'select',
		'options' => $mstatus_options,
		'label' => 'Filing Status'
	],
	'deds' => [
		'name' => 'deds',
		'type' => 'text',
		'size' => 2,
		'maxlength' => 2,
		'label' => 'Deductions'
	],
	'xfwt' => [
		'name' => 'xfwt',
		'type' => 'text',
		'size' => 12,
		'maxlength' => 12,
		'label' => 'Additional Withholding'
	],
	'wage' => [
		'name' => 'wage',
		'type' => 'text',
		'size' => 12,
		'maxlength' => 12,
		'label' => 'Salary/Wage',
		'required' => 1
	],
	'period' => [
		'name' => 'period',
		'type' => 'select',
		'options' => $period_options,
		'label' => 'Period'
	],
	's1' => [
		'name' => 's1',
		'type' => 'submit',
		'value' => 'Save'
	]
];
$form->set($fields);

view('Add Employee', [], 'empadd2.php', 'empadd');


