## Table of Contents

- [Introduction](#intro)
- [Limitations](#limitations)
- [Table Explanations](#tables)
- [Calculations](#calcs)
- [Forms](#forms)
- [Installation](#installation)
- [Assumptions](#assumptions)
- [Operation](#operation)
- [Disclaimer](#disclaimer)

-----

<a name="intro"></a>
### Introduction

Payroll is a pain to calculate manually, and quite complicated, at least
in the U.S. This program is designed to simplify this.

The primary purpose for this application is to calculate payroll related
taxes and generate reports with data needed to fill out tax forms. It is
designed specifically for U.S. taxes, and State of Florida taxes. The
design is such that you should be able to modify it for your specific
circumstances if they differ. It is written entirely in PHP. This version
is also able to generate PDF checks. But it does not link to a checkbook or
know anything about your checking account.

<a name="limitations"></a>
### Limitations

This software has some limitations.

- Certain deductions are not included or accounted for. These include, among
others, insurance, retirement, pension plans, wage garnishments, etc.
- Other than state unemployment taxes, there is no provision for any other
state or local taxes.
- There is no provision for overtime, sick time, double time, etc.


<a name="tables"></a>
### Table Explanations
We use the SQLite3 database to store and manage records. The following tables are involved.

#### employees (Employees table)

- empid - an eight character code for this employee
- empname - the employee's name (NOT USED)
- ssno - social security number
- mstatus - filing status (U.S. federal withholding) (e.g. married, single)
- gross - gross income for this employee (their "salary") per pay period (assumed to be weekly)
- xfwt - additional federal withholding to be withheld, above the usual amounts
- deds - number of deductions the employee is claiming (U.S. taxes)
- email - the employee's email address
- firstname - employee's first name
- middleinit - employee's middle initial; U.S. tax forms want this
- lastname - employee's last name
- wage - how much the employee gets paid per pay period
- period - pay period: hours, weeks, fortnights, month, quarter, year

#### taxes (Tax rates and bases, etc.)

- year - the year this record applies to (4 digits)
- taxtype - generally, "FWT", "SSEC", "MEDI", "FUTA" or "SUTA" (state unemployment tax)
- whopays - who pays-- employee or employer
- mstatus - filing status-- head of household, single, filing jointly, or not applicable
- hilimit - the upper income limit (if any) applicable to this record (0 if none)
- lolimit - the lower income limit applicable to this record
- base - the base tax amount for this record
- rate - the tax rate applicable for this record
- id - a serial field to identify each record uniquely
- whopays - employer or employee pays this tax

#### paychecks (Payroll checks written)

- empid - employee ID for the employee being paid by this check
- checkdate - date of the check, ISO notation
- checkno - check number
- gross - gross for this employee, as found in the premps table
- fwt - calculated federal withholding tax
- xfwt - additional federal withholding tax, as found in the premps table
- essec - calculated social security amount withheld from paycheck
- rssec - employer social security matching amount
- emedi - calculated medicare amount withheld from paycheck
- rmedi - employer medicare matching amount
- suta - calculated state unemployment tax amount
- futa - calculated federal unemployment tax amount
- net - net amount of the check to be paid to the employee

<a name="calcs"></a>
### Calculations

There are five basic tax types which are calculated. Although the tax
tables are identical in terms of their fields, taxes are not calculated
identically. Federal withholding requires a projected income calculation in
order to work. Others don't. Medicare taxes use a two-tiered system of
calculation which kicks in (presently) at $200,000.

Assuming the federal government does not radically change its rules, the
only thing which needs to be changed is the tax tables, which hold the 
tax rates for each tax. There is a screen for adjusting these each year.

The whole point of doing these calculations is for taxes. Although the
state of Florida does not require a tax breakdown on the employee's 
pay stub, we provide it. Our tax calculations (most of them) show up there.
The calculations also show up when filling out tax forms. There are menu
options for each of the tax forms. These give you the raw figures you
need to fill out the government forms.

<a name="forms"></a>
### Forms

This software will display (on screen) the information needed to fill out
all the forms you should need to fill out. It will also generate PDF
copies. Naturally, the forms are based on the forms <b>we</b> use, and the
most current version of those forms. Originally, we included line numbers
for each figure on each form, but these change almost yearly. So now we
only provide the figures, in roughly the order they will appear on the forms.

The following forms are provided for:

- 8109 - IRS monthly withholding taxes reporting booklet (we deposit monthly)
- 941 - quarterly withholding taxes reporting form
- UCT6 - Florida quarterly unemployment tax reporting form
- 940 - yearly IRS unemployment tax reporting form
- W2 - year IRS withholding taxes reporting form
- W3 - yearly W2 summary reporting form

<a name="installation"></a>
### Installation

This software is written in PHP, so you must install it where a web server may serve up
its web pages. Install it in any convenient directory your server can manage. It comes with
tax tables populated for this current year (2021). You may want to change these if you 
install in some other year. Once installed, you simply go to the index.php page in
the payroll directory, and you will see this page, along with the menu to the left.

Also note that this software requires another package of utilities I wrote,
called "grotto", which should be available where you found this package.
Install that package somewhere in your web hierarchy, preferrably outside
the payroll hierarchy. Then edit the `config/config.ini` file. Look for two
lines like this in that file:

```
incdir = "../grotto/"
libdir = "../grotto/"
```

Modify these lines to point to where you've installed the "grotto" package,
if different from the above.

<a name="assumptions"></a>
### Assumptions

There are certain assumptions we make with regard to the way you handle
your taxes. First, we assume that you deposit everything with the
governments on time. The forms allow for you to report when you still owe
the government money. We don't allow for that. We assume that by the time
you've filed the form, you've paid the taxes. We don't track IRS deposits
made with form 8109 monthly, so we assume you've made them. We similarly
assume you've made your federal unemployment tax deposit in a timely manner
(something which is easy to miss). We assume you enter tax rates based on
the yearly tax tables in IRS Circular E (which all employers should get a
copy of every year). We assume you pay your employees weekly, though you
may deviate from this. I've only tested it based on weekly payroll. 

More importantly, I'm not charging for this product, and I don't intend to
keep maintaining it. I don't currently have employees, so I don't need to
use it for payroll. Moreover, I'm not a tax guy, so I can't claim that
everything and every calculation this software makes is correct. It worked
for me. If you want to take this code and make a commercial product out of
it, feel free. I'd appreciate some credit for coming up with the original
code.

<a name="operation"></a>
### Operation

First, you must modify the existing tax tables for the current year, or add
new taxes for a new year. See [Tax Tables](tblshow.php) and
[Add Tax](tbladd.php).

Second, you must create employees. See [Add Employee](empadd.php).

Third, you must generate paychecks for whatever time period you choose.
This will be done weekly, monthly, or whatever your pay period is. See
[Paychecks/Generate](chkgen.php).

Last, you will generate government reports as needed. See the menu item
"Reports" for all the reports you can generate.

<a name="disclaimer"></a>
### Disclaimer

This application was written for our internal use. I carefully check the resulting figures each
time I file a report with the government, because even I don't trust the figures (and I wrote
the program). So if for some reason you're using this application, you're on your own. I didn't
write this application for you, and I can't check your figures. Use this application at your
own risk. I take no responsibility for anything bad, illegal or untoward that happens to you
or your company as a result of using this program. You're welcome to try to contact me to
ask questions, but even then, whatever happens still isn't my fault.

Good luck! ;-}

