<?php
include 'init.php';
$pr = model('payroll');
$months = $pr->get_months();
$years = $pr->get_years();
if ($years === FALSE) {
	emsg('F', 'No paychecks have been generated.');
	redirect('chkgen.php');
}

$month_options = [];
foreach ($months as $index => $month) {
	$month_options[] = ['lbl' => $month, 'val' => $index + 1];
}

$year_options = [];
foreach ($years as $year) {
	$year_options[] = ['lbl' => $year, 'val' => $year];
}

$fields = [
	'month' => [
		'name' => 'month',
		'type' => 'select',
		'options' => $month_options,
		'label' => 'Month'
	],
	'year' => [
		'name' => 'year',
		'type' => 'select',
		'options' => $year_options,
		'label' => 'Year'
	],
	's1' => [
		'name' => 's1',
		'type' => 'submit',
		'value' => 'Report'
	]
];
$form->set($fields);

view('Form 8109 (Monthly)', [], 'f8109-2.php', 'f8109');

