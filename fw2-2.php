<?php
include 'init.php';
$s1 = fork('s1', 'P', 'index.php');
$pr = model('payroll');

// get data for report(s)
$data = $pr->get_w2_data($_POST['year']);

// PDF report
$pdfr = model('forms');
$pdfr->formw2($_POST['year'], $data, $cfg['printdir'] . 'formw2.pdf');

// show it, baby!
view('Form W-2 Display', ['fw2' => $data], '', 'fw2-2');

