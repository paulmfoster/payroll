<?php

$links = [
	'Payroll' => [
		'Payroll Help' => 'index.php'
	],
	'Tax Tables' => [
		'Tax Table' => 'tblshow.php',
		'Add Tax' => 'tbladd.php'
	],
	'Employees' => [
		'Add Employee' => 'empadd.php',
		'List Employees' => 'emplist.php'
	],
	'Paychecks' => [
		'Layout' => 'layout.php',
		'List' => 'chklist.php',
		'Generate' => 'chkgen.php'
	],
	'Reports' => [
		'Checks/Reports' => 'print.php',
		'Form 8109' => 'f8109.php',
		'Form 941' => 'f941.php',
		'Form UCT6' => 'fuct6.php',
		'Form 940' => 'f940.php',
		'Form W2' => 'fw2.php',
		'Form W3' => 'fw3.php'
	],
	'Bugs' => [
		'Bug Report' => 'bugs.php',
		'Feature Request' => 'bugs.php'
	]
];

