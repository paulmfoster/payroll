<?php
include 'init.php';
$pr = model('payroll');

$years = $pr->get_years();
if ($years === FALSE) {
	emsg('F', 'No paychecks have been generated.');
	redirect('chkgen.php');
}
$year_options = [];
foreach ($years as $year) {
	$year_options[] = ['lbl' => $year, 'val' => $year];
}

$fields = [
	'year' => [
		'name' => 'year',
		'type' => 'select',
		'options' => $year_options,
		'label' => 'Year'
	],
	's1' => [
		'name' => 's1',
		'type' => 'submit',
		'value' => 'Report'
	]
];
$form->set($fields);

view('Form W-2 (Yearly)', [], 'fw2-2.php', 'pickyear');

