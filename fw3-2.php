<?php
include 'init.php';
$s1 = fork('s1', 'P', 'index.php');
$pr = model('payroll');

// get data for report(s)
$data = $pr->get_w3_data($_POST['year']);

// PDF report
$pdfr = model('forms');
$pdfr->formw3($_POST['year'], $data, $cfg['printdir'] . 'formw3.pdf');

// show it, baby!
view('Form W-3 Display', ['fw3' => $data], '', 'fw3-2');

